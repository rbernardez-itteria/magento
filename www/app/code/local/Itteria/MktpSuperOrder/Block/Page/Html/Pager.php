<?php
	class Itteria_MktpSuperOrder_Block_Page_Html_Pager extends Mage_Page_Block_Html_Pager{
	    public function __construct(){
			parent::__construct();
			$_itteriaAvailableLimit = array();
		}
		public function setAvailableLimit(array $limits){
		    $this->_itteriaAvailableLimit = $limits;
		}
		public function getAvailableLimit(){
		    return $this->_itteriaAvailableLimit;
		}
	}