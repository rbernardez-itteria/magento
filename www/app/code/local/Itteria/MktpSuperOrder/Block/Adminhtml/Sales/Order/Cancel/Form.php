<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Sales_Order_Cancel_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        
        $form = new Varien_Data_Form( array('id' => 'edit_form', 'action' => $this->getUrl('*/*/save'), 'method' => 'post', ));

        $form->setUseContainer(true);
        $this->setForm($form);

        $helper = Mage::helper('mktpsuperorder');
        $fieldset = $form->addFieldset('display', array('legend' => $helper->__('Justificació del rebuig')));

        $fieldset->addField('order_id', 'hidden', array(
                      'name' => 'order_id', 
                      'value' => $this->getOrder()->getId()
                    )
                  );
        $fieldset->addField('comentario_cancelacion', 'textarea', array(
                      'label'     => Mage::helper('mktpsuperorder')->__('Motiu del rebuig'),
                      'class'     => 'required-entry',
                      'required'  => true,
                      'name'      => 'comentario_cancelacion',
                    )
                  );

        return parent::_prepareForm();
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

}
