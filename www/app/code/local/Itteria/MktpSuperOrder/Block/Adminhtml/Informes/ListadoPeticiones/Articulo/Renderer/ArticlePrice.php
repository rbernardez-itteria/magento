<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoPeticiones_Articulo_Renderer_ArticlePrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input{

	public function render(Varien_Object $row){
		return number_format($row->getArticlePrice(),2,',','.') . ' €';
	}
}