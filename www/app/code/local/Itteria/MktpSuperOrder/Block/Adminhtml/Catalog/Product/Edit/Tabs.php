<?php

// Itteria - Clase que nos sirve para modificar los tabs que queremos que aparezcan en la ficha del producto
class Itteria_MktpSuperOrder_Block_Adminhtml_Catalog_Product_Edit_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs{

	protected function _prepareLayout(){
		parent::_prepareLayout();

		$roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
		$roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();

		if(strtolower($roleName) != 'administrators'){
			$this->removeTab('customer_options');
			$this->removeTab('customers_tags');
			$this->removeTab('tags');
			$this->removeTab('reviews');
			$this->removeTab('productalert');
			$this->removeTab('crosssell');
			$this->removeTab('related');
			$this->removeTab('inventory');
			$this->removeTab('upsell');
		}

		if(strtolower($roleName) == 'administrators' && strtolower($roleName) == 'supervisor'){
			$this->removeTab('set');
		}

		if(strtolower($roleName) == 'administrators' || strtolower($roleName) == 'supervisor'){
			$this->addTab('logproduct', array(
			    'label'     => Mage::helper('catalog')->__('Historial de canvis'),
			    'content'   => $this->_translateHtml($this->getLayout()->createBlock('mktpsuperorder/adminhtml_catalog_product_edit_tab_log')->toHtml())
			));
		}
	}
}