<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoPeticiones_Articulo_Grid extends Itteria_MktpExportGrid_Block_Adminhtml_Widget_Grid {
    public function __construct() {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('mktpsuperorder_informes_listadopeticiones_articulo_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
     
    protected function _prepareCollection() {
        $collection = Mage::getModel('mktpsuperorder/informe_articulo')->getCollection()
                            ->addFieldToFilter('informe_id', array('eq' => $this->getRequest()->getParam('informe_id')));

        $this->setCollection($collection);     
        return parent::_prepareCollection();
    }
     
    protected function _prepareColumns() {
      
        $this->addColumn('article_sku', array(
            'header'=> Mage::helper('sales')->__('SKU'),
            'type'  => 'text',
            'index' => 'article_sku',
        ));

        $this->addColumn('article_name', array(
            'header' => Mage::helper('sales')->__('Nom'),
            'index' => 'article_name',
            'type' => 'text',
        ));

        $this->addColumn('article_brand', array(
            'header' => Mage::helper('sales')->__('Marca'),
            'type'  => 'text',
            'index' => 'article_brand',
        ));

        $this->addColumn('article_model', array(
            'header' => Mage::helper('sales')->__('Model'),
            'type' => 'text',
            'index' => 'article_model',
        ));

        $this->addColumn('article_producer', array(
            'header' => Mage::helper('sales')->__('Fabricant'),
            'type' => 'text',
            'index' => 'article_producer',
        ));

        $this->addColumn('article_cum', array(
            'header' => Mage::helper('sales')->__('U.M'),
            'type' => 'number',
            'index' => 'article_cum',
        ));

        $this->addColumn('article_qty', array(
            'header' => Mage::helper('sales')->__('Quantitat'),
            'type' => 'number',
            'width' => '100px',
            'index' => 'article_qty',
        ));

        $this->addColumn('article_price', array(
            'header'=> Mage::helper('sales')->__('Preu'),
            'index' => 'article_price',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_articulo_renderer_articlePrice',
        ));

        $this->addColumn('article_subtotal', array(
            'header'=> Mage::helper('sales')->__('Subtotal'),
            'index' => 'article_subtotal',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_articulo_renderer_articleSubtotal',
        ));

        $this->addColumn('article_address', array(
            'header'=> Mage::helper('sales')->__('Adreça'),
            'type'  => 'text',
            'index' => 'article_address',
        ));

        $this->setDefaultSort('article_sku');
        $this->setDefaultDir('desc');


        $this->addExportType('*/*/exportCsvListadoPeticionesArticulo', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcelListadoPeticionesArticulo', Mage::helper('sales')->__('Excel XML'));
        $this->addExportType('*/*/exportpdfListadoPeticionesArticulo', Mage::helper('sales')->__('PDF'));
        
        return parent::_prepareColumns();
    }

    public function getGridUrl(){
        return $this->getUrl('*/*/gridListadoPeticionesArticulo', array('_current'=>true));
    }
}