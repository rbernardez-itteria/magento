<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoPeticiones extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		// Itteria - Nombre del modulo
	    $this->_blockGroup = 'mktpsuperorder';
	    
	    // Itteria - Nombre del block y no del controller
	    $this->_controller = 'adminhtml_Informes_listadoPeticiones';
	    $this->_headerText = $this->__('Informe de peticions');
	    parent::__construct();

	    // Itteria - Eliminamos el botón de añadir
	    $this->_removeButton('add');
	}
}