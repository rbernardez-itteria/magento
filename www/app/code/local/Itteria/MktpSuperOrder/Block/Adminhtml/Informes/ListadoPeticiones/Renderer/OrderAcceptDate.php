<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoPeticiones_Renderer_OrderAcceptDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input{

	public function render(Varien_Object $row){
		if($row->getOrderAcceptDate() == '0000-00-00 00:00:00'){
			return '-';
		}else{
			return date('d/m/Y', strtotime($row->getOrderAcceptDate()));
		}
	}
}