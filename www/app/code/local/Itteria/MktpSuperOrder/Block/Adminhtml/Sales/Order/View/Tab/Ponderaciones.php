<?php
class Itteria_MktpSuperOrder_Block_Adminhtml_Sales_Order_View_Tab_Ponderaciones
            extends Mage_Adminhtml_Block_Template
            implements Mage_Adminhtml_Block_Widget_Tab_Interface {
        
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('itteria/sales/order/view/tab/ponderaciones.phtml');
    }

    public function getTabLabel() {
        return $this->__('Pressupostos per la comanda');
    }

    public function getTabTitle() {
        return $this->__('Pressupostos per la comanda');
    }

    public function canShowTab() {
        $roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
        $roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();
        
        if(strtolower($roleName) == 'administrators' || strtolower($roleName) == 'supervisor'){
            return true;
        }else{
            return false;
        }
    }

    public function isHidden() {
        $roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
        $roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();
        
        if(strtolower($roleName) == 'administrators' || strtolower($roleName) == 'supervisor'){
            return false;
        }else{
            return true;
        }
    }

    public function getOrder(){
        return Mage::registry('current_order');
    }

    /**
     *  Retorna una estructura como la siguiente, incluyendo toda la información de los rechazos
     * 
     *  $rechazos = array(
     *       0 => array(
     *           'empresa' => 'Nombre empresa 1',
     *           'cif'     => '12345679A',
     *           'montiu'  => '9999.99 €',
     *           'comment' => 'Aquí va el motivo de rechazo de la comanda 1',
     *       ),
     *       1 => array(
     *           'empresa' => 'Nombre empresa 2',
     *           'cif'     => '987654321A',
     *           'montiu'  => '9999.99 €',
     *           'comment' => 'Aquí va el motivo de rechazo de la comanda 2',
     *       ),
     *   );
     * 
     */
    public function getRechazos() {
        
        // Obtenemos los rechazos para el pedido actual
        $rechazos = $this->getOrder()->getInfoRechazos();

        // Itteria - Obtenemos todos los datos del pedido
        $order_real = Mage::getModel('sales/order')->load($this->getOrder()->getId());

        // Montamos la estructura de datos necesaria
        $info = array();
        foreach ($rechazos as $rechazo) {

            // Itteria - Obtenemos la tienda
            $store = Mage::getModel('core/store')->load($rechazo['proveedor']);

            // Si no existe este store, saltamos
            if (!is_object($store) || $store->getId() == '') { continue; }

            // Itteria - Obtenemos todos los comentarios del pedido
            $history = $order_real->getStatusHistoryCollection();

            // Itteria - Buscamos el comentario de rechazo de ese proveedor
            foreach ($history as $comment) {
                if(strpos($comment->getComment(),$store->getName()) != FALSE){
                    $comment_parts = explode('Preu Comanda:', $comment->getComment());
                    $comment_reason = explode('següent raó: ', $comment_parts[0]);
                }
            }

            $info[] = array(
                'empresa' => $store->getName(),
                'cif'     => $store->getGroup()->getName(),
                'montiu'  => number_format($rechazo['total'], 2, "," , ".") . ' €',
                'comment' => $comment_reason[1],
                'total'   => $rechazo['total'],
            );
        }
        
        return $info;
    } 
    
    /**
     *  Retorna una estructura como la siguiente, incluyendo toda la información de las ofertas
     * 
     *  $ponderacion = array(
     *       0 => array(
     *           'empresa' => 'Nombre empresa 1',
     *           'cif'     => '12345679A',
     *           'importe' => '9999.99 €',
     *       ),
     *       1 => array(
     *           'empresa' => 'Nombre empresa 2',
     *           'cif'     => '987654321A',
     *           'importe' => '9999.99 €',
     *       ),
     *   ); 
     *  
     */
    protected function getPonderacion() {
        
        // Obtenemos la última ponderación de este pedido
        $idsRechazo  = $this->getOrder()->getIdsRechazos();
        $ponderacion = $this->getOrder()->getUltimaPonderacion();
        $proveedores = $ponderacion->getProveedores();

        // Itteria - Obtenemos los datos del IVA
        $tax = Mage::getModel('tax/calculation_rate')->loadByCode('IVA');
        
        // Montamos el array de info
        $info = array();
        foreach ($proveedores as $proveedor) {

            // Itteria - Obtenemos la tienda
            $store = Mage::getModel('core/store')->load($proveedor['store_id']);
            
            // Si no existe este store, saltamos
            if (!is_object($store) || $store->getId() == '') { continue; }
            
            // Si es rechazado, no figura en esta tabla
            if (in_array($proveedor['store_id'], $idsRechazo)) { continue; }
            
            $info[] = array(
                'empresa' => $proveedor['proveedor'],
                'cif'     => $store->getGroup()->getName(),
                'importe' => number_format($proveedor['subtotal'] * (($tax['rate']+100)/100), 2, "," , ".") . ' €',
                'total'   => $proveedor['subtotal'],
            );
        }

        return $info;
    } 
    
}