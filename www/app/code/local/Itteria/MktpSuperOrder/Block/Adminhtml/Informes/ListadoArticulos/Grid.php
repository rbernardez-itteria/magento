<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoArticulos_Grid extends Itteria_MktpExportGrid_Block_Adminhtml_Widget_Grid{

	public function __construct(){
	    parent::__construct();
	    $this->setId('productGrid');
	    $this->setDefaultSort('entity_id');
	    $this->setDefaultDir('DESC');
	    $this->setSaveParametersInSession(true);
	    $this->setUseAjax(true);
	    $this->setVarNameFilter('product_filter');
	}

	protected function _getStore(){
	    $storeId = (int) $this->getRequest()->getParam('store', 0);
	    return Mage::app()->getStore($storeId);
	}

	protected function _prepareCollection()
	{
	    $store = $this->_getStore();
	    $collection = Mage::getModel('catalog/product')->getCollection()
	        ->addAttributeToSelect('sku')
	        ->addAttributeToSelect('name')
	        ->addAttributeToSelect('attribute_set_id')
	        ->addAttributeToSelect('type_id');

	    if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
	        $collection->joinField('qty',
	            'cataloginventory/stock_item',
	            'qty',
	            'product_id=entity_id',
	            '{{table}}.stock_id=1',
	            'left');
	    }
	    if ($store->getId()) {
	        //$collection->setStoreId($store->getId());
	        $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
	        $collection->addStoreFilter($store);
	        $collection->joinAttribute(
	            'name',
	            'catalog_product/name',
	            'entity_id',
	            null,
	            'inner',
	            $adminStore
	        );
	        $collection->joinAttribute(
	            'custom_name',
	            'catalog_product/name',
	            'entity_id',
	            null,
	            'inner',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'status',
	            'catalog_product/status',
	            'entity_id',
	            null,
	            'inner',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'visibility',
	            'catalog_product/visibility',
	            'entity_id',
	            null,
	            'inner',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'price',
	            'catalog_product/price',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'precio_maximo',
	            'catalog_product/precio_maximo',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'codigo_cpv',
	            'catalog_product/codigo_cpv',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'desc_cpv',
	            'catalog_product/desc_cpv',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'fabricante',
	            'catalog_product/fabricante',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'marca',
	            'catalog_product/marca',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'modelo',
	            'catalog_product/modelo',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	        $collection->joinAttribute(
	            'unid_venta',
	            'catalog_product/unid_venta',
	            'entity_id',
	            null,
	            'left',
	            $store->getId()
	        );
	    }
	    else {
	        $collection->addAttributeToSelect('price');
	        $collection->addAttributeToSelect('precio_maximo');
	        $collection->addAttributeToSelect('codigo_cpv');
	        $collection->addAttributeToSelect('desc_cpv');
	        $collection->addAttributeToSelect('fabricante');
	        $collection->addAttributeToSelect('marca');
	        $collection->addAttributeToSelect('modelo');
	        $collection->addAttributeToSelect('unid_venta');
	        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
	        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
	    }

	    $this->setCollection($collection);

	    parent::_prepareCollection();
	    $this->getCollection()->addWebsiteNamesToResult();
	    return $this;
	}

	protected function _addColumnFilterToCollection($column)
	{
	    if ($this->getCollection()) {
	        if ($column->getId() == 'websites') {
	            $this->getCollection()->joinField('websites',
	                'catalog/product_website',
	                'website_id',
	                'product_id=entity_id',
	                null,
	                'left');
	        }
	    }
	    return parent::_addColumnFilterToCollection($column);
	}

	protected function _prepareColumns(){
	    $this->addColumn('entity_id',
	        array(
	            'header'=> Mage::helper('catalog')->__('ID'),
	            'width' => '50px',
	            'type'  => 'number',
	            'index' => 'entity_id',
	    ));
	    $this->addColumn('name',
	        array(
	            'header'=> Mage::helper('catalog')->__('Name'),
	            'index' => 'name',
	    ));

	    $this->addColumn('sku',
	        array(
	            'header'=> Mage::helper('catalog')->__('SKU'),
	            'width' => '80px',
	            'index' => 'sku',
	    ));

	    $this->addColumn('codigo_cpv',
	        array(
	            'header'=> Mage::helper('catalog')->__('Codi CPV'),
	            'type'  => 'text',
	            'index' => 'codigo_cpv',
	    ));

	    $this->addColumn('desc_cpv',
	        array(
	            'header'=> Mage::helper('catalog')->__('Descripció CPV'),
	            'type'  => 'text',
	            'index' => 'desc_cpv',
	    ));

	    $this->addColumn('unid_venta',
	        array(
	            'header'=> Mage::helper('catalog')->__('Grups per unitats de venda'),
	            'type'  => 'number',
	            'index' => 'unid_venta',
	    ));

	    $this->addExportType('*/*/exportCsvListadoArticulos', Mage::helper('sales')->__('CSV'));
	    $this->addExportType('*/*/exportExcelListadoArticulos', Mage::helper('sales')->__('Excel XML'));
	    $this->addExportType('*/*/exportpdfListadoArticulos', Mage::helper('sales')->__('PDF'));

	    return parent::_prepareColumns();
	}

	public function getRowUrl($row){
	    return $this->getUrl('*/*/peticionArticulo', array(
	        'article_id'=>$row->getId())
	    );
	}

	public function getGridUrl(){
	    return $this->getUrl('*/*/gridListadoArticulos', array('_current'=>true));
	}
}