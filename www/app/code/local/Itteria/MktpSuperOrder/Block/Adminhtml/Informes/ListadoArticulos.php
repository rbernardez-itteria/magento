<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoArticulos extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		// Itteria - Nombre del modulo
	    $this->_blockGroup = 'mktpsuperorder';
	    
	    // Itteria - Nombre del block y no del controller
	    $this->_controller = 'adminhtml_Informes_listadoArticulos';
	    $this->_headerText = $this->__("Informe d'articles");
	    parent::__construct();

	    // Itteria - Eliminamos el botón de añadir
	    $this->_removeButton('add');
	}
}