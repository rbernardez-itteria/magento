<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Comandas_ListadoComandas_Grid extends Itteria_MktpExportGrid_Block_Adminhtml_Widget_Grid {
  
    public function __construct() {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('mktpsuperorder_informes_listadocomandas_grid');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
     
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status', array('eq' => 'Finalitzat'));

        $this->setCollection($collection);
        
        $c = parent::_prepareCollection();
        return $c;
    }
     
    protected function _prepareColumns() {
      
        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'number',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Proveïdor'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
                'renderer' => 'adminhtml/sales_order_grid_renderer_store',
            ));
        }

        $this->addColumn('customer', array(
            'header' => Mage::helper('sales')->__('Encarregat de material'),
            'index' => 'customer_firstname',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->setDefaultSort('real_order_id');
        $this->setDefaultDir('desc');


        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));
        
        $this->removeColumn('shipping_name');
        $this->removeColumn('base_grand_total');

        return parent::_prepareColumns();
    }

    public function getGridUrl(){
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}