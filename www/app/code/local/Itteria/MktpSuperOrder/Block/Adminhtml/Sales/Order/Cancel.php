<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Sales_Order_Cancel extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'order_id';
        $this->_controller = 'adminhtml_sales_order';
        $this->_blockGroup = 'mktpsuperorder';
        $this->_mode = 'cancel';

        $this->_updateButton('save', 'label', Mage::helper('mktpsuperorder')->__('Rebutjar comanda'));
        $this->_removeButton('delete');
    }

    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function getHeaderText()
    {
        $header = Mage::helper('sales')->__('Rebutjar comanda #%s', $this->getOrder()->getRealOrderId());
        return $header;
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/sales_order/view', array('order_id'=>$this->getOrder()->getOrderId()));
    }
}
