<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Informes_ListadoArticulos_Peticion_Grid extends Itteria_MktpExportGrid_Block_Adminhtml_Widget_Grid {
  
    public function __construct() {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('mktpsuperorder_informes_listadopeticiones_grid');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }
     
    protected function _prepareCollection(){
        $collection = Mage::getModel('mktpsuperorder/informe_articulo')->getCollection()
                            ->addFieldToFilter('article_id', array('eq' => $this->getRequest()->getParam('article_id')));

        $informes = array();
        foreach ($collection as $articulo) {
            if(!in_array($articulo['informe_id'], $informes)){
                array_push($informes, $articulo['informe_id']);
            }
        }

        $collection = Mage::getModel('mktpsuperorder/informe')->getCollection()->addFieldToFilter('id', array('in' => $informes));

        $this->setCollection($collection);     
        return parent::_prepareCollection();
    }
     
    protected function _prepareColumns() {
      
        $this->addColumn('informe_id', array(
            'header'=> Mage::helper('sales')->__('Id #'),
            'width' => '50px',
            'index' => 'id',
        ));

        $this->addColumn('order_code', array(
            'header'=> Mage::helper('sales')->__('Comanda #'),
            'index' => 'order_code',
        ));

        $this->addColumn('order_created_date', array(
            'header' => Mage::helper('sales')->__('Data de creació'),
            'index' => 'order_created_date',
            'type' => 'date',
        ));

        $this->addColumn('order_accept_date', array(
            'header' => Mage::helper('sales')->__("Data d'acceptació"),
            'type'  => 'date',
            'index' => 'order_accept_date',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderAcceptDate',
        ));

        $this->addColumn('order_refuse_date', array(
            'header' => Mage::helper('sales')->__('Data de rebuig'),
            'type' => 'date',
            'index' => 'order_refuse_date',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderRefuseDate',
        ));

        $this->addColumn('order_confirm_date', array(
            'header' => Mage::helper('sales')->__('Data de confirmació'),
            'type' => 'date',
            'index' => 'order_confirm_date',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderConfirmDate',
        ));

        $this->addColumn('order_sent_date', array(
            'header' => Mage::helper('sales')->__("Data d'enviament completat"),
            'type' => 'date',
            'index' => 'order_sent_date',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderSentDate',
        ));

        $this->addColumn('order_complete_date', array(
            'header' => Mage::helper('sales')->__('Data de comanda finalitzada'),
            'type' => 'date',
            'index' => 'order_complete_date',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderCompleteDate',
        ));

        $this->addColumn('order_state', array(
            'header'=> Mage::helper('sales')->__('Estat'),
            'index' => 'order_state',
            'type'  => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('order_user', array(
            'header'=> Mage::helper('sales')->__('Encarregat de material'),
            'type'  => 'text',
            'index' => 'order_user',
        ));

        $this->addColumn('order_provider', array(
            'header'=> Mage::helper('sales')->__('Proveïdor'),
            'type'  => 'text',
            'index' => 'order_provider',
        ));

        $this->addColumn('order_rejected', array(
            'header'=> Mage::helper('sales')->__('Rebutjada'),
            'type'  => 'text',
            'index' => 'order_rejected',
        ));

        $this->addColumn('order_total', array(
            'header' => Mage::helper('sales')->__('Total'),
            'index' => 'order_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
            'renderer' => 'mktpsuperorder/adminhtml_informes_listadoPeticiones_renderer_orderTotal',
        ));

        $this->setDefaultSort('informe_id');
        $this->setDefaultDir('desc');

        $this->addExportType('*/*/exportCsvListadoArticulosPeticion', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcelListadoArticulosPeticion', Mage::helper('sales')->__('Excel XML'));
        $this->addExportType('*/*/exportpdfListadoArticulosPeticion', Mage::helper('sales')->__('PDF'));
        
        return parent::_prepareColumns();
    }

    public function getGridUrl(){
        return $this->getUrl('*/*/gridListadoArticulosPeticion', array('_current'=>true));
    }
}