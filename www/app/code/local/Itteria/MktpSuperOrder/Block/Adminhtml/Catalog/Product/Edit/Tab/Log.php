<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Catalog_Product_Edit_Tab_Log extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct() {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('mktpsuperorder_catalog_product_edit_tab_log');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }
     
    protected function _prepareCollection() {
        $collection = Mage::getModel('mktpproduct/log')->getCollection()->addFieldToFilter('product_id', array('eq' => $this->getRequest()->getParam('id')));

        $this->setCollection($collection);
        
        $c = parent::_prepareCollection();
        return $c;
    }
     
    protected function _prepareColumns() {
      
        $this->addColumn('log_id', array(
            'header'=> Mage::helper('sales')->__('Log id'),
            'width' => '10px',
            'type'  => 'text',
            'index' => 'id',
        ));

        $this->addColumn('product_id', array(
            'header'=> Mage::helper('sales')->__('Producte id'),
            'width' => '10px',
            'type'  => 'text',
            'index' => 'product_id',
        ));

        $this->addColumn('event', array(
            'header'=> Mage::helper('sales')->__('Event'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'event',
        ));

        $this->addColumn('user_id', array(
            'header'=> Mage::helper('sales')->__('Usuari'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'user_id',
        ));

        $this->addColumn('value_before', array(
            'header'=> Mage::helper('sales')->__('Valor anterior'),
            'width' => '280px',
            'type'  => 'text',
            'index' => 'value_before',
        ));

        $this->addColumn('value_after', array(
            'header'=> Mage::helper('sales')->__('Valor actualitzat'),
            'width' => '280px',
            'type'  => 'text',
            'index' => 'value_after',
        ));

        $this->addColumn('created_at', array(
            'header'=> Mage::helper('sales')->__('Realitzat el'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'created_at',
        ));

        return parent::_prepareColumns();
    }
}
