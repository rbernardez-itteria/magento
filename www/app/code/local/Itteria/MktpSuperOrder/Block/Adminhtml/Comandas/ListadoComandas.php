<?php

class Itteria_MktpSuperOrder_Block_Adminhtml_Comandas_ListadoComandas extends Mage_Adminhtml_Block_Widget_Grid_Container {

	public function __construct() {
		// Itteria - Nombre del modulo
	    $this->_blockGroup = 'mktpsuperorder';
	    
	    // Itteria - Nombre del block y no del controller
	    $this->_controller = 'adminhtml_Comandas_listadoComandas';
	    $this->_headerText = $this->__('Adjudicació de comandes');
	    parent::__construct();

	    // Itteria - Eliminamos el botón de añadir
	    $this->_removeButton('add');
	}
}