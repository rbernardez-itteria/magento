<?php

class Itteria_MktpSuperOrder_Block_Sales_Order_Shipment_View extends Mage_Adminhtml_Block_Sales_Order_Shipment_View{

	public function __construct(){

	    parent::__construct();

	    $this->_removeButton('save');
	    $this->_removeButton('print');
	}
}