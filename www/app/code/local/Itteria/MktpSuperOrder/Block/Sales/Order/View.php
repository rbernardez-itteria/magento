<?php
	class Itteria_MktpSuperOrder_Block_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View{
	    public function __construct(){
			parent::__construct();
			$this->_removeButton('order_ship');

			$order = $this->getOrder();

			$roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
			$roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();



			if ($this->_isAllowedAction('ship') && $order->canShip()
			    && !$order->getForcedDoShipmentWithInvoice() && $order->getStatus() == 'processing' && strtolower($roleName) != 'supervisor') {
			    $this->_addButton('order_ship', array(
			        'label'     => Mage::helper('sales')->__('Ship'),
			        'onclick'   => 'setLocation(\'' . $this->getShipUrl() . '\')',
			        'class'     => 'go'
			    ));
			}
		}
	}
