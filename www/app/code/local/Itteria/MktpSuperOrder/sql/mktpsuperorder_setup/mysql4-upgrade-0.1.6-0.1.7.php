<?php

$installer = $this;
$installer->startSetup();


$w = $this->_conn;
$w->addColumn($installer->getTable('sales/order'), 
	'respuesta', 
	array(
	    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
	    'length' => 255,
	    'nullable' => true,
	    'default' => '',
	    'comment' => 'Tiempo máximo respuesta',
	    )
	);
    
$installer->endSetup();