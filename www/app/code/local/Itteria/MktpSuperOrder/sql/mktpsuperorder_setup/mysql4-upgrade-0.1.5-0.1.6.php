<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/ponderacion')};
    CREATE TABLE {$this->getTable('mktpsuperorder/ponderacion')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL,
        `winner` int(11) NOT NULL,
        `amount` decimal(10,4) NOT NULL,
        `excluded` text NULL,
        `unavailable` text NULL,
        `type` varchar(64) NOT NULL,
        `extra_data` text NOT NULL,
        `created_at` timestamp NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/ponderacion_proveedor')};
    CREATE TABLE {$this->getTable('mktpsuperorder/ponderacion_proveedor')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `parent_id` int(11) unsigned NOT NULL,
        `store_id` int(11) NOT NULL,
        `proveedor` varchar(128) NOT NULL,
        `subtotal` decimal(10,4) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/ponderacion_item')};
    CREATE TABLE {$this->getTable('mktpsuperorder/ponderacion_item')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `ponderacion_id` int(11) NOT NULL,
        `proveedor_id` int(11) NOT NULL,
        `product_id` int(11) NOT NULL,
        `qty` int(11) NOT NULL,
        `price` decimal(10,4) NOT NULL,
        `row_total` decimal(10,4) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    
$installer->endSetup();