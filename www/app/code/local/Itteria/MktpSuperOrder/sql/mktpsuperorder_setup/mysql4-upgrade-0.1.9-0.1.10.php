<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `mktpsuperorder_informe` ADD order_rejected VARCHAR(256) AFTER order_provider;
");

$installer->endSetup();