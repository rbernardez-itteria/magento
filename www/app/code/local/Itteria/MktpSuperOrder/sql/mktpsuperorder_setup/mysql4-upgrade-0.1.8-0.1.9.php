<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/informe')};
    CREATE TABLE {$this->getTable('mktpsuperorder/informe')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL,
        `order_code` varchar(64) NOT NULL,
        `order_created_date` timestamp NOT NULL,
        `order_accept_date` timestamp NOT NULL,
        `order_refuse_date` timestamp NOT NULL,
        `order_confirm_date` timestamp NOT NULL,
        `order_sent_date` timestamp NOT NULL,
        `order_complete_date` timestamp NOT NULL,
        `order_state` varchar(64) NOT NULL,
        `order_user` varchar(64) NOT NULL,
        `order_provider` varchar(64) NOT NULL,
        `order_total` varchar(64) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/informe_articulo')};
    CREATE TABLE {$this->getTable('mktpsuperorder/informe_articulo')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `order_id` int(11) NOT NULL,
        `informe_id` int(11) NOT NULL,
        `article_id` int(11) NOT NULL,
        `article_sku` varchar(64) NOT NULL,
        `article_name` varchar(128) NOT NULL,
        `article_qty` int(11) NOT NULL,
        `article_brand` varchar(128) NOT NULL,
        `article_model` varchar(128) NOT NULL,
        `article_producer` varchar(128) NOT NULL,
        `article_cum` int(11) NOT NULL,
        `article_price` decimal(10,4) NOT NULL,
        `article_subtotal` decimal(10,4) NOT NULL,
        `article_address` varchar(128) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");
    
$installer->endSetup();