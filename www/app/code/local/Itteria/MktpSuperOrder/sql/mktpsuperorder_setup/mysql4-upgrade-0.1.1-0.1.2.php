<?php

$installer = $this;
$installer->startSetup();


$w = $this->_conn;
$w->addColumn($installer->getTable('sales/order'), 
              'presupuesto', 
              array(
                    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'default' => 'Pendiente',
                    'comment' => 'Presupuesto aceptado',
                    )
              );
    
$installer->endSetup();