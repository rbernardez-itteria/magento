<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('mktpsuperorder/budget')};
    CREATE TABLE {$this->getTable('mktpsuperorder/budget')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `expediente` varchar(255) NOT NULL,
        `oficina` varchar(255) NOT NULL,
        `organo` varchar(255) NOT NULL,
        `unidad` varchar(255) NOT NULL,
        `facturacion` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$installer->endSetup();
