<?php

class Itteria_MktpSuperOrder_Model_Cron extends Mage_Core_Model_Abstract {
      
    /**
     * Comprueba si ha expirado el plazo de aceptación del proveedor y reasigna la comanda
     */
    function checkProvider() {
        Mage::log(__METHOD__, null, 'superorders.log');
        
        // Obtenemos todos los pedidos que cumplan:
        //     Estén en estado pending (Pendiente proveedor)
        
        $now = time();
        $orders = Mage::getModel('sales/order')->getCollection()
                        ->addAttributeToSelect('entity_id')
                        ->addAttributeToSelect('created_at')
                        ->addAttributeToSelect('respuesta')
                        ->addAttributeToSelect('increment_id')
                        ->addFieldToFilter('status', 'pending')
                        ->setOrder('entity_id', 'asc');
        
        // Procesamos todos los pedidos necesarios
        foreach ($orders as $order) {
            
            // Todas las horas obtenidas con time() o getCreatedAt() retornan
            // la hora según la hora del servidor.
            
            // Timestamp del pedido en segundos
            $orderTime = strtotime($order->getCreatedAt());
            
            // Tiempo límite para este pedido en segundos
            $limiteHoras = $order->getRespuesta();
            $limiteSegundos = 3600 * $limiteHoras;
            
            Mage::log("Comprobando pedido: {$order->getIncrementId()}", null, 'superorders.log');
            Mage::log("Ahora         : " . date('Y-m-d H:i:s', $now), null, 'superorders.log');
            Mage::log("Ahora (seg.)  : " . $now, null, 'superorders.log');
            Mage::log("Pedido        : " . $orderTime, null, 'superorders.log');
            Mage::log("Pedido (seg.) : " . $orderTime, null, 'superorders.log');
            Mage::log("Limite (horas): " . $limiteHoras, null, 'superorders.log');
            Mage::log("Limite (seg.) : " . $limiteSegundos, null, 'superorders.log');
            Mage::log("Diff (horas.) : " . ($now - $orderTime) / 3600, null, 'superorders.log');
            Mage::log("Diff (seg.)   : " . ($now - $orderTime), null, 'superorders.log');
            Mage::log("", null, 'superorders.log');
            
            // Comprobamos si supera el límite
            if(($now - $orderTime) > $limiteSegundos) {

                Mage::log('Procesando pedido pendiente proveedor: ' . $order->getIncrementId(), null, 'superorders.log');
                $order = Mage::getModel('sales/order')->load($order->getEntityId());
                $comment = "El proveïdor " . $order->getStore()->getName() . " no ha acceptat la comanda en el temps límit";

                // Itteria - Generación de las notificaciones por email
                if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_cancel_comanda')){

                    // Itteria - Elegimos la plantilla que vamos a usar
                    $template_id = 'emails_cancel_time_comanda_template';

                    // Itteria - Elegimos los destinatarios
                    $email_to = array();
                    
                    // Itteria - Email nuevo proveedor
                    // Itteria - Obtenemos el rol del siguiente proveedor
                    $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $order->getStoreId()))->getData();
                    // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
                    $users = Mage::getModel('admin/user')->getCollection();
                    foreach ($users as $user) {
                        if($user->getRole()->getId() == $role[0]['role_id']){
                            $email_to[0] = $user->getEmail();
                        }
                    }
                    $email_to[1] = 'supervisoritteria@gmail.com';
                    // Itteria - Cargamos la plantilla del email
                    $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

                    // Itteria - Cargamos las variables que nos van a hacer falta en el email
                    $email_template_variables = array(
                        'order' => $order,
                    );

                    // Itteria - Configuramos el resto de ajustes del mail
                    $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
                    $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
                    $email_template->setSenderName($sender_name);
                    $email_template->setSenderEmail($sender_email); 

                    // Itteria - Enviamos los emails a los destinatarios
                    foreach ($email_to as $_from) {
                        $email_template->send($_from, $_from, $email_template_variables);
                    }
                }
                $proveedor = Mage::getModel('core/store')->load($order->getStoreId())->getName();
                $order->siguienteProveedor(Itteria_MktpSuperOrder_Model_Ponderacion::RECHAZO, $comment);
                $order->actualizarProveedorInforme($proveedor);
                Mage::log('Hecho', null, 'superorders.log');
            }
        }
    }
}
  
