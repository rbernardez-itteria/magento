<?php
// Itteria - Función que cambia el proveedor en el pedido creado en función de la ponderación
class Itteria_MktpSuperOrder_Model_Observer {
    
    public function ponderaPedido() {

        // Itteria - Consigue todos los productos del carrito
        $cartHelper = Mage::helper('checkout/cart');
        $items = $cartHelper->getCart()->getItems();

        // Itteria - Obtenemos el conjunto de websites dadas de alta
        $websites = Mage::app()->getWebsites();

        // Itteria - Guardamos todos los ids de los productos del carrito
        $opciones_generales = array();
        foreach($items as $item){
            $itemId = $item->getItemId();
            $productCollection[] = array(
                                'id' => $item->getProductId(), 
                                'cantidad' => $item->getQty()
                                );
            // Itteria: Obtenemos todas las opciones que tiene asociada el producto
            $total_opciones = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
            $total_opciones = $total_opciones['options'];
            $total_opciones['product_id'] = $item->getProductId();
            array_push($opciones_generales, $total_opciones);
        }


        // Itteria - Recorremos todas las tiendas para averiguar cual es el carrito mínimo
        $websiteDisponible = array();
        foreach ($websites as $website){
            $pedido_tienda = 0;

            // Itteria - Sumamos los carritos de cada proveedor
            foreach ($productCollection as $product){
                // Itteria - Comprobamos los proveedores que tiene activo el producto
                $websiteDisponible = Mage::getModel('catalog/product')->load($product['id'])->getWebsiteIds();
                // Itteria - Si el producto no está activo el pedido no se pondera para ese proveedor
                if(!in_array($website->getId(), $websiteDisponible)){
                    $pedido_tienda = 0;
                    break;
                }
                $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($product['id']);
                if($productWebsite->getPrice() == 0){
                    $pedido_tienda = 0;
                    break;
                }
                $pedido_tienda += $productWebsite->getPrice() * $product['cantidad'];
            }

            // Itteria - Guardamos los proveedores para posteriormente ordenarlos
            if($pedido_tienda != 0){
                $proveedores[] = array(
                                'pedido' => $pedido_tienda,
                                'id' => $website->getId()
                                );
            }
        }

        // Itteria - Ordenamos los proveedores en función del pedido
        foreach ($proveedores as $key => $row) {
            $pedido[$key]  = $row['pedido']; 
        }
        array_multisort($pedido, SORT_ASC, $proveedores);

        // Itteria - Si el pedido no supera el mínimo no se podrá terminar el pedido
        if($proveedores[0]['pedido'] < 200){
            Mage::throwException("La seva comanda no supera l'import mínim de 200 euros.  Incorpori més articles per poder finalitzar-la");
        }
    }
    
    public function chooseVendor() {

        // Itteria - Obtenemos el último pedido creado
        $order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastOrderId());

        // Siguiente proveedor
        $order->siguienteProveedor(Itteria_MktpSuperOrder_Model_Ponderacion::NUEVO_PEDIDO, 'Nova comanda');

        // Itteria - Creación del informe asociado al pedido
        $order->crearInforme();
    }

    public function comprobacionPedidoCompleto($observer){

        // Itteria - Obtenemos el pedido
        $order = $observer->getEvent()->getOrder();
        if($order->getStatusLabel() == 'Enviat per proveïdor'){
            // Itteria - Obtenemos el informe asociado a dicho pedido
            $informes = Mage::getModel('mktpsuperorder/informe')->getCollection()->addFieldToFilter('order_id', array('eq' => $order->getId()));
            foreach ($informes as $informe) {
                $informe_actual = Mage::getModel('mktpsuperorder/informe')->load($informe->getData('id'));
            }
            if($informe_actual->getOrderSentDate() == '0000-00-00 00:00:00'){
                $order->actualizarEstadoInforme('enviada');
            }
        }
    }

    public function adminSystemConfigChangedState(Varien_Event_Observer $observer){
        
        // Itteria - Envío de los emails
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_change_state')){
            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'admin_config_change_state_template';

            // Itteria - Elegimos los proveedores
            $proveedores = array();
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            // Itteria - Emails a los proveedores
            $cont = 0;
            $users = Mage::getModel('admin/user')->getCollection();
            foreach ($users as $user) {
                $proveedores[$cont] = $user->getEmail();
                $cont++;
            }

            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);
            $message = 'El catàleg electrònic ha canviat l’estat i ara podeu modificar els preus que teniu establerts / ja no podeu modificar els preus que teniu establerts mitjançant el vostre compte de l’aplicació.';
            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'message' => $message,
                'supervisor' => $supervisor->getEmail(),
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los proveedores
            foreach ($proveedores as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }

            // Itteria - Elegimos los destinatarios
            $encargados_almacen = array();
            // Itteria - Emails a los proveedores
            $cont = 0;
            $users = Mage::getModel('customer/customer')->getCollection();
            foreach ($users as $user) {
                $encargados_almacen[$cont] = $user->getEmail();
                $cont++;
            }
            $message = 'El catàleg electrònic ha canviat l’estat i ara no podeu confirmar comandes / ja podeu confirmar comandes mitjançant el vostre compte de l’aplicació.';
            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'message' => $message,
                'supervisor' => $supervisor->getEmail(),
            );

            // Itteria - Enviamos los emails a los proveedores
            foreach ($encargados_almacen as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }
    }
}