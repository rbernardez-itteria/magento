<?php 

class Itteria_MktpSuperOrder_Model_Pdf extends Mage_Sales_Model_Order_Pdf_Abstract {
    
    const MARGIN_TOP = 50;
    const MARGIN_BOTTOM = 100;
    const MARGIN_LEFT = 50;
    const MARGIN_RIGHT = 50;
    
    const SEP_V = 20;
    const SEP_H = 20;
    
    const TABLE_ROW_HEIGHT = 20;
    //const TABLE_PAD_X = 10;
    //const TABLE_PAD_Y = 10;
    
    const FONT_SIZE = 10;
    const FONT_SIZE_SMALL = 9;
    
    // Tabla de rechazos
    var $colsRechazos = array(
        'empresa' => array( 'width'  => 125, 'text' => 'Empresa',    ),
        'cif'     => array( 'width'  => 60,  'text' => 'CIF',         ),
        'data'  => array( 'width'  => 100,  'text' => 'Data rebuig',      ),
        'comment' => array( 'width'  => 215, 'text' => 'Montiu', ),
    );
    
    // Tabla de ponderación
    var $colsPonderacion = array(
        'empresa' => array( 'width'  => 200, 'text' => 'Empresa',       ),
        'cif'     => array( 'width'  => 100, 'text' => 'CIF',           ),
        'importe' => array( 'width'  => 200, 'text' => 'Import total sense IVA', ),
    );
    
    // Tabla de items
    var $colsItems = array(
        'codigo'          => array( 'width'  => 85,    'text' => 'Codi article',    ),
        'descripcion'     => array( 'width'  => 110,    'text' => 'Descripció',      ),
        'cjt'             => array( 'width'  => 30,    'text' => 'Qtat', ),
        'marca'           => array( 'width'  => 85,    'text' => 'Marca',           ),
        'modelo'          => array( 'width'  => 100,    'text' => 'Model',           ),
        'fabricante'      => array( 'width'  => 100,    'text' => 'Fabricant',       ),
        'importe'         => array( 'width'  => 70,    'text' => 'Import',          ),
        'lloc_lliurament' => array( 'width'  => 110,    'text' => 'Lloc lliurament',),
        'data_lliurament' => array( 'width'  => 70,    'text' => 'Data lliurament', ),
    );
    
    var $pdf = null;
    var $fontRegular = null;
    var $fontBold = null;
    var $curPage = null; 
    var $curPageFormat = null; 
    
    var $pageW = null;
    var $pageH = null;
    var $pageUsedW = null;
    var $pageUsedH = null;
    var $pageCenterX = null;
    
    var $curH = null;
    
    public function getPdf($orderId = null) {

        $this->order = Mage::getModel('sales/order')->load($orderId);
        
        // Creamos el PDF
        $this->pdf = new Zend_Pdf();
        $this->fontRegular = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
        $this->fontBold = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);

        // Datos generales de la petición (todo excepto la tabla de items)
        $this->drawGeneralInfo($orderId);

        // Pintamos las páginas de items
        $this->drawItems();
        
        // Devolvemos el PDF formado
        return $this->pdf;
    }

    protected function drawGeneralInfo($orderId = null) {
        
        // Comenzamos una nueva página. Esta información siempre en vertical.
        $this->startNewPage(Zend_Pdf_Page::SIZE_A4);

        //  Dejamos espacio al logotipo
        $this->moveAndCheckForNewPage(1.5 * self::SEP_V);

        // Título
        $string = 'CATCOMP';
        $this->drawTextCenter($string, self::MARGIN_LEFT, $this->curH, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE);
        $this->moveAndCheckForNewPage(1.5 * self::SEP_V);
        
        // Itteria - Obtenemos la información del pedido
        $order = Mage::getModel('sales/order')->load($orderId);

        // Encabezado
        $string = 'INFORME PER LA TRAMITACIÓ ADMINISTRATIVA DE LA PETICIÓ ' . $order->getIncrementId();
        $this->drawTextCenter($string, self::MARGIN_LEFT, $this->curH, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE);
        $this->moveAndCheckForNewPage(2 * self::SEP_V);

        // Itteria - Obtenemos la persona creadora del pedido
        $customer_id = $order->getCustomerId();
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $centroCoste = Mage::getModel('centrocoste/centrocoste')->load($customer->getCentrocoste());

        // Itteria - Obtenemos el presupuesto
        $presupuesto = Mage::getModel('mktpsuperorder/budget')->load($order->getPresupuesto());

        // Itteria - Obtenemos la fecha de aceptación del pedido

        // Itteria - Obtenemos todos los comentarios del pedido
        $history = $order->getStatusHistoryCollection(true);
        // Itteria - Buscamos el comentario de rechazo de ese proveedor
        foreach ($history as $comment) {
            if(strpos($comment->getComment(),$order->getStore()->getName()) != FALSE){
                $comment_parts = explode('el proveïdor', $comment->getComment());
                $comment_reason = explode('El día', $comment_parts[0]);
                $fecha_comentario = $comment->getCreatedAtDate();
            }
        }

        // Preparamos las líneas de info básica
        $lines = array (
            'CODI DE LA PETICIÓ' => $order->getIncrementId(),
            'PERSONA RESPONSABLE DE LA PETICIÓ' => $customer->getName(),
            'CENTRE GESTOR ASSOCIAT A LA PERSONA RESPONSABLE' => $centroCoste->getName(),
            'NÚM. EXPEDIENTE GEEC' => $presupuesto->getExpediente(),
            'CODI D\'OFICINA COMPTABLE' => $presupuesto->getOficina(),
            'CODI D\'ÒRGAN GESTOR' => $presupuesto->getOrgano(),
            'CODI D\'UNITAT TRAMITADORA' => $presupuesto->getUnidad(),
            'DATA DE CÀLCUL D\'OFERTES' => str_replace('.', ':', $order->getCreatedAtDate()),
            'EMPRESA SUBMINISTRADORA' => $order->getStore()->getName(),
            'CIF EMPRESA SUBMINISTRADORA' => $order->getStore()->getGroup()->getName(),
            'DATA D\'ACCEPTACIÓ' => str_replace('.', ':', $comment_reason[1]),
        );
        
        // Y las pintamos
        foreach ($lines as $title => $text) {
            $this->drawLineWithTitleAndText(self::MARGIN_LEFT, $this->curH, $title, $text);
            $this->moveAndCheckForNewPage(self::SEP_V);
        }
        
        // Si alguna empresa ha rechazado la comanda, lo ponemos a continuación
        if ($empresas = $this->getRechazos()) {
            
            // Pintamos el título
            $title = '(LLISTA) EMPRESES QUE HAN REBUTJAT SERVIR LA COMANDA';
            $this->drawLineWithTitleAndText(self::MARGIN_LEFT, $this->curH, $title, '');
            $this->moveAndCheckForNewPage(1.5*self::SEP_V);
            
            // Pintamos la tabla
            $this->drawTable(self::MARGIN_LEFT, $this->curH, $this->colsRechazos, $empresas);
            $this->moveAndCheckForNewPage(self::SEP_V);
        }
        
        // Preparamos las líneas de precios

        // Itteria - Calculamos los impuestos
        $tax = Mage::getModel('tax/calculation_rate')->loadByCode('IVA');

        $lines = array (
            'IMPORT TOTAL DE LA PETICIÓ A ADJUDICAR SENSE IVA' => number_format($order->getGrandTotal() / (($tax['rate']+100)/100), 2, "," , ".") . ' €',
            'IMPORT TOTAL DE LA PETICIÓ A ADJUDICAR AMB IVA' => number_format($order->getGrandTotal(), 2, "," , ".") .  ' €',
        );
        
        // Y las pintamos
        foreach ($lines as $title => $text) {
            $this->drawLineWithTitleAndText(self::MARGIN_LEFT, $this->curH, $title, $text);
            $this->moveAndCheckForNewPage(self::SEP_V);
        }
        
        // Otras ofertas para esta comanda
        if ($ponderaciones = $this->getPonderacion()) {
            
            // Itteria - Eliminamos los totales de las ponderaciones
            $info = array();
            foreach ($ponderaciones as $ponderacion) {
                $info[] = array(
                    'empresa' => $ponderacion['empresa'],
                    'cif'     => $ponderacion['cif'],
                    'importe' => $ponderacion['importe'],
                );
            }

            // Pintamos el título
            $title = 'RELACIÓ D\'OFERTES D\'EMPRESES';
            $this->drawLineWithTitleAndText(self::MARGIN_LEFT, $this->curH, $title, '');
            $this->moveAndCheckForNewPage(1.5*self::SEP_V);
            
            // Pintamos la tabla
            $this->drawTable(self::MARGIN_LEFT, $this->curH, $this->colsPonderacion, $info);
            //$this->moveAndCheckForNewPage(self::SEP_V);
        }

        $this->endCurrentPage();
    }

    protected function drawItems() {
        
        // Obtenemos todos los items del pedido y pintamos la tabla
        if ($items = $this->getItems()) {
            
            // Comenzamos una nueva página. Esta información siempre en horizontal.
            $this->startNewPage(Zend_Pdf_Page::SIZE_A4_LANDSCAPE); 

            // Pintamos el título
            $title = 'LLISTA D\'ARTICLES';
            $this->drawLineWithTitleAndText(self::MARGIN_LEFT, $this->curH, $title, '');
            $this->moveAndCheckForNewPage(1.5*self::SEP_V);
        
            // Dibujamos esta página
            $this->drawTable(self::MARGIN_LEFT, $this->curH, $this->colsItems, $items);
            
            // Fin de la última página
            $this->endCurrentPage();
        }
    }
    
    protected function moveAndCheckForNewPage($h, $createNewPage = true) {
            
        // Movemos el cursor la altura indicada    
        $this->curH = $this->curH - $h;
        
        // Si estamos fuera de márgenes, saltamos de página
        if ($this->curH < self::MARGIN_BOTTOM) {
            if ($createNewPage) {
                $this->endCurrentPage();
                $this->startNewPage();
            }
            return true;
        }
        
        return false;
    }
    
    protected function startNewPage($format = null) {
        
        // Si no hay formato, usamos el actual
        if ($format == null) {
            $format = $this->curPageFormat;
        }

        Mage::log($format);
        
        // Creamos la primera página
        $this->curPage = new Zend_Pdf_Page($format);
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE);
        
        // Apuntamos el formato actual por si hay saltos de página
        $this->curPageFormat = $format;
        
        // La página tiene 595 x 842 pt, siendo 0,0 la esquina inferior izquierda
        $this->pageW  = $this->curPage->getWidth();
        $this->pageH = $this->curPage->getHeight();
        
        // Ancho y alto usados
        $this->pageUsedW  = $this->pageW  - (2*self::MARGIN_LEFT);
        $this->pageUsedH = $this->pageH - (2*self::MARGIN_TOP);
        $this->pageCenterX = $this->pageUsedW / 2;

        $image = Mage::getConfig()->getOptions()->getMediaDir().DS.'logo.jpg';
        if (is_file($image)) {
            $image = Zend_Pdf_Image::imageWithPath($image);
            if($format == '595:842:'){
                $this->drawImageV($image);
            }else{
                $this->drawImageH($image);
            }
        }
        
        // Comenzamos pintando por arriba
        $this->curH = $this->pageH;
        $this->curH = $this->curH - self::MARGIN_TOP;
    }
    
    protected function endCurrentPage() {
        
        // Nota al pie
        //  $this->drawFooter();

        // Añadimos la página al PDF
        $this->pdf->pages[] = $this->curPage;
    }
    
    protected function drawTableHeaders($xIni, $xFin, $headers) {
        
        // Linea inicial de la tabla
        $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
        
        // Pintamos los encabezados
        foreach ($headers as $key => $col) {
            $this->drawTextCenter($col['text'], $col['x'], $this->curH, $col['width'], self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
        }
        $this->moveAndCheckForNewPage(self::TABLE_ROW_HEIGHT);

        // Linea bajo los títulos
        $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
    }
    
    protected function drawTable($x, $y, $headers, $data) {
        
        // Datos de inicio de la tabla
        $xIni = $xCur = $x;
        $yIni = $y;

        // Itteria - Incremento que haremos a la altura de la celda dependiendo del número de líneas de una celda
        $alturaCelda = 0;
        
        // Calculamos más datos de las columnas y pintamos los encabezados
        foreach ($headers as $key => $col) {
            
            // Calculamos posición X de inicio de la columna
            $headers[$key]['x'] = $xCur;
            $xCur = $xCur + $col['width'];
        }
        $xFin = $xCur;
        
        // Dibujamos encabezados
        $this->drawTableHeaders($xIni, $xFin, $headers);
        
        // Pintamos los datos
        foreach ($data as $key => $row) {
            
            // Dibujamos las cabeceras
            foreach ($row as $colName => $text) {
                
                // Datos de la columna 
                $col = $headers[$colName];
                
                // Y pintamos el texto

                // Itteria - Si estamos en alguno de los campos suceptibles de ser más grandes los tratamos individualmentes
                if($col['text'] == 'Descripció' || $col['text'] == 'Lloc lliurament' || $col['text'] == 'Montiu' || $col['text'] == 'Fabricant'){
                    
                    // Itteria - Extraemos todas las palabras del texto individualmente
                    $palabras = explode(' ', $text);
                    // Itteria - Establecemeos la sangria
                    $sangria = 20;
                    $incremento_sangria = ($col['text'] == 'Montiu') ? 15 : 20;
                    // Itteria - Establecemos el limite de caracteres
                    $caracteres = ($col['text'] == 'Montiu') ? 55 : 22;
                    // Itteria - Establecemos el incremento de altura de la celda
                    $incremento_altura_celda = ($col['text'] == 'Montiu') ? 10 : 12;
                    // Itteria - Creamos la variable donde almacenaremos las diferentes lineas en las que se dividira el texto de la celda
                    $linea = '';
                    
                    // Itteria - Recorremos todas las palabras
                    foreach ($palabras as $palabra) {
                        if($linea == ''){
                            $linea = $palabra;
                        }else{
                            // Itteria - Vamos concatenando palabras hasta llegar al tope de caracteres de una linea
                            $linea_concatenada = $linea . ' ' . $palabra;
                            if(strlen($linea_concatenada) < $caracteres){
                                $linea = $linea_concatenada;
                            }else{
                                $this->drawText($linea, $col['x']+10, $this->curH, $sangria, self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                                $alturaCelda = $alturaCelda + $incremento_altura_celda;
                                $linea = $palabra;
                                $sangria = $sangria + $incremento_sangria;
                            }
                        }
                    }
                    $this->drawText($linea, $col['x']+10, $this->curH, $sangria, self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                }
                else{
                    if($col['text'] == 'Import'){
                        $text = number_format($text, 2, "," , ".") . '€';
                    }
                    $this->drawTextCenter($text, $col['x'], $this->curH, $col['width'], self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                }
            }

            // Movemos a la siguiente fila, comprobamos si estamos fuera de límites y cambiamos de página 
            // si es necesario. En este caso cambio de página manual, ya que tenemos que pintar las líneas antes
            // Aqui
            $pagejump = $this->moveAndCheckForNewPage(self::TABLE_ROW_HEIGHT+$alturaCelda, false);
            $alturaCelda = 0;
            
            // Linea bajo los la fila
            $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
            
            // Si nos salimos de la página o si es el último item, dibujamos líneas verticales y saltamos
            if ($pagejump || $row == end($data)) {
                
                // Líneas verticales
                foreach ($headers as $key => $col) {
                    $this->curPage->drawLine($col['x'], $yIni, $col['x'],  $this->curH);
                }
                $this->curPage->drawLine($xFin, $yIni, $xFin,  $this->curH);
                
                // Si es salto de página y no es fin de datos, iniciamos la nueva página
                if ($pagejump && $row != end($data)) {
                    
                    // Cerramos una página y abrimos otra
                    $this->endCurrentPage();
                    $this->startNewPage();
                    
                    // Recalculamos x, y iniciales
                    $yIni = $this->curH; 
        
                    // Dibujamos encabezados
                    $this->drawTableHeaders($xIni, $xFin, $headers);
                }
            }
        }
    }
    
    protected function drawText($string, $x, $y, $lineHeight) {
        $this->curPage->drawText($string, $x, $y - $lineHeight /1.5, 'UTF-8');
    }

    protected function drawImageV($string) {
        $this->curPage->drawImage($string, 220,770,400,805);
    }
    protected function drawImageH($string) {
        $this->curPage->drawImage($string, 330,550,510,585);
    }
    
    protected function drawTextCenter($string, $x, $y, $columnWidth, $lineHeight, Zend_Pdf_Resource_Font $font, $fontSize) {
        $posX = $this->getAlignCenter($string, $x, $columnWidth, $font, $fontSize);
        $this->drawText($string, $posX, $y, $lineHeight);
    }
    
    protected function drawLineWithTitleAndText($x, $y, $title, $text) {
        
        // Título
        $title = $title . ':';
        $this->curPage->setFont($this->fontBold, self::FONT_SIZE);
        $this->drawText($title, $x, $y, self::SEP_V);
        $w = $this->widthForStringUsingFontSize($title, $this->fontBold, self::FONT_SIZE);
        
        // Texto
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE);
        $this->drawText($text,  $x + $w + self::SEP_H, $y, self::SEP_V);
    }
    
    protected function drawFooter() {
        
        // En letra pequeña
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE_SMALL);
        
        $string = "Aquesta comanda respon a les necessitats de la unitat de compra pel que fa al material d'oficina";
        $this->drawTextCenter($string, self::MARGIN_LEFT, self::MARGIN_BOTTOM - self::SEP_V - 40, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE_SMALL);
        
        $string = "imprescindile per fer les tasques necessàries en els diversos processos que la unitat ha de dur a terme";
        $this->drawTextCenter($string, self::MARGIN_LEFT, self::MARGIN_BOTTOM - self::SEP_V - 55, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE_SMALL);
    }

    /**
     *  Retorna una estructura como la siguiente, incluyendo toda la información de los rechazos
     * 
     *  $rechazos = array(
     *       0 => array(
     *           'empresa' => 'Nombre empresa 1',
     *           'cif'     => '12345679A',
     *           'montiu'  => '9999.99 €',
     *           'comment' => 'Aquí va el motivo de rechazo de la comanda 1',
     *       ),
     *       1 => array(
     *           'empresa' => 'Nombre empresa 2',
     *           'cif'     => '987654321A',
     *           'montiu'  => '9999.99 €',
     *           'comment' => 'Aquí va el motivo de rechazo de la comanda 2',
     *       ),
     *   );
     * 
     */
    protected function getRechazos() {
        
        // Obtenemos los rechazos para el pedido actual
        $rechazos = $this->order->getInfoRechazos();

        // Itteria - Obtenemos todos los datos del pedido
        $order_real = Mage::getModel('sales/order')->load($this->order->getId());

        // Montamos la estructura de datos necesaria
        $info = array();
        foreach ($rechazos as $rechazo) {

            // Itteria - Obtenemos la tienda
            $store = Mage::getModel('core/store')->load($rechazo['proveedor']);

            // Itteria - Obtenemos todos los comentarios del pedido
            $history = $order_real->getStatusHistoryCollection();

            // Itteria - Buscamos el comentario de rechazo de ese proveedor
            foreach ($history as $comment) {
                if(strpos($comment->getComment(),$store->getName()) != FALSE){
                    $comment_parts = explode('Preu Comanda:', $comment->getComment());
                    $comment_reason = explode('següent raó: ', $comment_parts[0]);
                    $fecha_comentario = $comment->getCreatedAtDate();
                }

            }

            $info[] = array(
                'empresa' => $store->getName(),
                'cif'     => $store->getGroup()->getName(),
                'data'  => $fecha_comentario,
                'comment' => $comment_reason[1],
            );
        }
        
        return $info;
    } 

    /**
     *  Retorna una estructura como la siguiente, incluyendo toda la información de las ofertas
     * 
     *  $ponderacion = array(
     *       0 => array(
     *           'empresa' => 'Nombre empresa 1',
     *           'cif'     => '12345679A',
     *           'importe' => '9999.99 €',
     *       ),
     *       1 => array(
     *           'empresa' => 'Nombre empresa 2',
     *           'cif'     => '987654321A',
     *           'importe' => '9999.99 €',
     *       ),
     *   ); 
     *  
     */
    protected function getPonderacion() {
        
        // Obtenemos la última ponderación de este pedido
        $idsRechazo  = $this->order->getIdsRechazos();
        $ponderacion = $this->order->getUltimaPonderacion();
        $proveedores = $ponderacion->getProveedores();

        // Itteria - Obtenemos los datos del IVA
        $tax = Mage::getModel('tax/calculation_rate')->loadByCode('IVA');
        
        // Montamos el array de info
        $info = array();
        foreach ($proveedores as $proveedor) {

            // Itteria - Obtenemos la tienda
            $store = Mage::getModel('core/store')->load($proveedor['store_id']);
            
            // Si es rechazado, no figura en esta tabla
            if (in_array($proveedor['store_id'], $idsRechazo)) { continue; }
            
            $info[] = array(
                'empresa' => $proveedor['proveedor'],
                'cif'     => $store->getGroup()->getName(),
                'importe' => number_format($proveedor['subtotal'] * (($tax['rate']+100)/100), 2, "," , ".") . ' €',
                'total'   => $proveedor['subtotal'],
            );
        }

        // Itteria - Ordenamos las ponderaciones en funcion de su precio total
        foreach ($info as $clave => $fila) {
            $total[$clave] = $fila['total'];
        }
        array_multisort($total, SORT_ASC, $info);

        unset($info[0]);

        return $info;
    } 

    protected function getItems() {
        
        // Itteria - Obtenemos todos los datos del pedido
        $order_real = Mage::getModel('sales/order')->load($this->order->getId());

        $items_package = array();

        // Itteria - Recorremos todos los items del pedido
        foreach($order_real->getAllItems() as $item){
            
            // Itteria - Obtenemos las caracteristicas del producto del proveedor
            $producto = Mage::getModel('catalog/product')->setStoreId($order_real->getStoreId())->load($item->getProduct()->getId());

            // Itteria - Obtenemos la dirección y la fecha de entrega deseada
            $fecha_entrega = '-';
            $_customOptions = $item->getProductOptions();
            foreach($_customOptions['options'] as $_option){
                if($_option['label'] == "Adreça d'enviament"){
                    $direccion = $_option['value'];
                }
                if($_option['label'] == "Data de lliurament desitjada"){
                    $fecha_entrega = $_option['value'];
                }
            }

            $items_package[] = array(
                'codigo'          => $item->getSku(),
                'descripcion'     => $item->getProduct()->getName(),
                'cjt'             => intval($item->getData('qty_ordered')),
                'marca'           => $producto->getData('marca'),
                'modelo'          => $producto->getData('modelo'),
                'fabricante'      => $producto->getData('fabricante'),
                'importe'         => $item->getData('qty_ordered') * $item->getPrice(),  
                'lloc_lliurament' => $direccion,
                'data_lliurament' => $fecha_entrega,
            );
        }
        
        
        return $items_package;
    } 
}
    