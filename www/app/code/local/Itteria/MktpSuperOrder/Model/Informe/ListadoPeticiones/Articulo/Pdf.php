<?php 

class Itteria_MktpSuperOrder_Model_Informe_ListadoPeticiones_Articulo_Pdf extends Mage_Sales_Model_Order_Pdf_Abstract {
    
    const MARGIN_TOP = 50;
    const MARGIN_BOTTOM = 100;
    const MARGIN_LEFT = 50;
    const MARGIN_RIGHT = 50;
    
    const SEP_V = 20;
    const SEP_H = 20;
    
    const TABLE_ROW_HEIGHT = 20;
    //const TABLE_PAD_X = 10;
    //const TABLE_PAD_Y = 10;
    
    const FONT_SIZE = 10;
    const FONT_SIZE_SMALL = 9;
    
    // Tabla de items
    var $colsItems = array(
        '0' => array( 'width' => 60, 'text' => 'Codi Article'),
        '1' => array( 'width' => 130, 'text' => 'Nom'),
        '2' => array( 'width' => 80, 'text' => 'Marca'),
        '3' => array( 'width' => 60, 'text' => 'Model'),
        '4' => array( 'width' => 80, 'text' => 'Fabricant'),
        '5' => array( 'width' => 30, 'text' => 'U.M'),
        '6' => array( 'width' => 50, 'text' => 'Quantitat'),
        '7' => array( 'width' => 60, 'text' => 'Precio'),
        '8' => array( 'width' => 80, 'text' => 'Subtotal'),
        '9' => array( 'width' => 130, 'text' => 'Lloc lliurament'),
    );
    
    var $pdf = null;
    var $fontRegular = null;
    var $fontBold = null;
    var $curPage = null; 
    var $curPageFormat = null; 
    
    var $pageW = null;
    var $pageH = null;
    var $pageUsedW = null;
    var $pageUsedH = null;
    var $pageCenterX = null;
    
    var $curH = null;
    
    public function getPdf($datos = 1) {
        
        // Creamos el PDF
        $this->pdf = new Zend_Pdf();
        $this->fontRegular = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
        $this->fontBold = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);

        // Datos generales de la petición (todo excepto la tabla de items)
        // $this->drawGeneralInfo();

        // Pintamos las páginas de items
        $this->drawItems($datos);
        
        // Devolvemos el PDF formado
        return $this->pdf;
    }

    protected function drawGeneralInfo($orderId = null) {
        
        // Comenzamos una nueva página. Esta información siempre en vertical.
        $this->startNewPage(Zend_Pdf_Page::SIZE_A4);

        // Título
        $string = 'INFORME PER CONJUNT DE ARTICLES';
        $this->drawTextCenter($string, self::MARGIN_LEFT, $this->curH, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE);
        $this->moveAndCheckForNewPage(1.5 * self::SEP_V);

        $this->endCurrentPage();
    }

    protected function drawItems($datos) {
            
        // Comenzamos una nueva página. Esta información siempre en horizontal.
        $this->startNewPage(Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $image = Mage::getConfig()->getOptions()->getMediaDir().DS.'logo.jpg';
        if (is_file($image)) {
            $image = Zend_Pdf_Image::imageWithPath($image);  
            $this->drawImage($image);
        } 
        // Título
        $string = 'INFORME PER CONJUNT DE ARTICLES';
        $this->drawTextCenter($string, self::MARGIN_LEFT, $this->curH, $this->pageUsedW, self::SEP_V, $this->fontRegular, 14);
        $this->moveAndCheckForNewPage(1.5 * self::SEP_V);
    
        // Dibujamos esta página
        $this->drawTable(self::MARGIN_LEFT, $this->curH, $this->colsItems, $datos);
        
        // Fin de la última página
        $this->endCurrentPage();
    }
    
    protected function moveAndCheckForNewPage($h, $createNewPage = true) {
            
        // Movemos el cursor la altura indicada    
        $this->curH = $this->curH - $h;
        
        // Si estamos fuera de márgenes, saltamos de página
        if ($this->curH < self::MARGIN_BOTTOM) {
            if ($createNewPage) {
                $this->endCurrentPage();
                $this->startNewPage();
            }
            return true;
        }
        
        return false;
    }
    
    protected function startNewPage($format = null) {
        
        // Si no hay formato, usamos el actual
        if ($format == null) {
            $format = $this->curPageFormat;
        }
        
        // Creamos la primera página
        $this->curPage = new Zend_Pdf_Page($format);
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE);
        
        // Apuntamos el formato actual por si hay saltos de página
        $this->curPageFormat = $format;
        
        // La página tiene 595 x 842 pt, siendo 0,0 la esquina inferior izquierda
        $this->pageW  = $this->curPage->getWidth();
        $this->pageH = $this->curPage->getHeight();
        
        // Ancho y alto usados
        $this->pageUsedW  = $this->pageW  - (2*self::MARGIN_LEFT);
        $this->pageUsedH = $this->pageH - (2*self::MARGIN_TOP);
        $this->pageCenterX = $this->pageUsedW / 2; 
        
        // Comenzamos pintando por arriba
        $this->curH = $this->pageH;
        $this->curH = $this->curH - self::MARGIN_TOP;
    }
    
    protected function endCurrentPage() {
        
        // Nota al pie
        $this->drawFooter();

        // Añadimos la página al PDF
        $this->pdf->pages[] = $this->curPage;
    }
    
    protected function drawTableHeaders($xIni, $xFin, $headers) {
        
        // Linea inicial de la tabla
        $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
        
        // Pintamos los encabezados
        foreach ($headers as $key => $col) {
            $this->drawTextCenter($col['text'], $col['x'], $this->curH, $col['width'], self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
        }
        $this->moveAndCheckForNewPage(self::TABLE_ROW_HEIGHT);

        // Linea bajo los títulos
        $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
    }
    
    protected function drawTable($x, $y, $headers, $data) {
        
        // Datos de inicio de la tabla
        $xIni = $xCur = $x;
        $yIni = $y;

        // Itteria - Incremento que haremos a la altura de la celda dependiendo del número de líneas de una celda
        $alturaCelda = 0;
        
        // Calculamos más datos de las columnas y pintamos los encabezados
        foreach ($headers as $key => $col) {
            
            // Calculamos posición X de inicio de la columna
            $headers[$key]['x'] = $xCur;
            $xCur = $xCur + $col['width'];
        }
        $xFin = $xCur;
        
        // Dibujamos encabezados
        $this->drawTableHeaders($xIni, $xFin, $headers);
        
        // Pintamos los datos
        foreach ($data as $key => $row) {

            // Dibujamos las cabeceras
            foreach ($row as $colName => $text) {

                // Datos de la columna
                if(isset($headers[$colName])){
                    $col = $headers[$colName];
                }

                if($col['text'] == 'Nom' || $col['text'] == 'Lloc lliurament' || $col['text'] == 'Fabricant' || $col['text'] == 'Marca' || $col['text'] == 'Model'){
                    
                    // Itteria - Extraemos todas las palabras del texto individualmente
                    $palabras = explode(' ', $text);
                    // Itteria - Establecemeos la sangria
                    $sangria = 20;
                    $incremento_sangria = ($col['text'] == 'Montiu') ? 15 : 20;
                    // Itteria - Establecemos el limite de caracteres
                    $caracteres = ($col['text'] == 'Marca' || $col['text'] == 'Model' || $col['text'] == 'Fabricant') ? 12 : 28;
                    // Itteria - Establecemos el incremento de altura de la celda
                    $incremento_altura_celda = ($col['text'] == 'Montiu') ? 10 : 10;
                    // Itteria - Creamos la variable donde almacenaremos las diferentes lineas en las que se dividira el texto de la celda
                    $linea = '';
                    
                    // Itteria - Recorremos todas las palabras
                    foreach ($palabras as $palabra) {
                        if($linea == ''){
                            $linea = $palabra;
                        }else{
                            // Itteria - Vamos concatenando palabras hasta llegar al tope de caracteres de una linea
                            $linea_concatenada = $linea . ' ' . $palabra;
                            if(strlen($linea_concatenada) < $caracteres){
                                $linea = $linea_concatenada;
                            }else{
                                $linea = utf8_encode($linea);
                                $this->drawText($linea, $col['x']+10, $this->curH, $sangria, self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                                $alturaCelda = $alturaCelda + $incremento_altura_celda;
                                $linea = $palabra;
                                $sangria = $sangria + $incremento_sangria;
                            }
                        }
                    }
                    $linea = utf8_encode($linea);
                    $this->drawText($linea, $col['x']+10, $this->curH, $sangria, self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                }
                else{
                    $text = utf8_encode($text);
                    $this->drawTextCenter($text, $col['x'], $this->curH, $col['width'], self::TABLE_ROW_HEIGHT, $this->fontRegular, self::FONT_SIZE);
                }
            }

            // Movemos a la siguiente fila, comprobamos si estamos fuera de límites y cambiamos de página 
            // si es necesario. En este caso cambio de página manual, ya que tenemos que pintar las líneas antes
            // Aqui
            $pagejump = $this->moveAndCheckForNewPage(self::TABLE_ROW_HEIGHT+$alturaCelda, false);
            $alturaCelda = 0;
            
            // Linea bajo los la fila
            $this->curPage->drawLine($xIni, $this->curH, $xFin,  $this->curH);
            
            // Si nos salimos de la página o si es el último item, dibujamos líneas verticales y saltamos
            if ($pagejump || $row == end($data)) {
                
                // Líneas verticales
                foreach ($headers as $key => $col) {
                    $this->curPage->drawLine($col['x'], $yIni, $col['x'],  $this->curH);
                }
                $this->curPage->drawLine($xFin, $yIni, $xFin,  $this->curH);
                
                // Si es salto de página y no es fin de datos, iniciamos la nueva página
                if ($pagejump && $row != end($data)) {
                    
                    // Cerramos una página y abrimos otra
                    $this->endCurrentPage();
                    $this->startNewPage();
                    
                    // Recalculamos x, y iniciales
                    $yIni = $this->curH; 
        
                    // Dibujamos encabezados
                    $this->drawTableHeaders($xIni, $xFin, $headers);
                }
            }
        }
    }
    
    protected function drawText($string, $x, $y, $lineHeight) {
        $this->curPage->drawText($string, $x, $y - $lineHeight /1.5, 'UTF-8');
    }

    protected function drawImage($string) {
        $this->curPage->drawImage($string, 330,550,510,585);
    }
    protected function drawTextCenter($string, $x, $y, $columnWidth, $lineHeight, Zend_Pdf_Resource_Font $font, $fontSize) {
        $posX = $this->getAlignCenter($string, $x, $columnWidth, $font, $fontSize);
        $this->drawText($string, $posX, $y, $lineHeight);
    }
    
    protected function drawLineWithTitleAndText($x, $y, $title, $text) {
        
        // Título
        $title = $title . ':';
        $this->curPage->setFont($this->fontBold, self::FONT_SIZE);
        $this->drawText($title, $x, $y, self::SEP_V);
        $w = $this->widthForStringUsingFontSize($title, $this->fontBold, self::FONT_SIZE);
        
        // Texto
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE);
        $this->drawText($text,  $x + $w + self::SEP_H, $y, self::SEP_V);
    }
    
    protected function drawFooter() {
        /*
        // En letra pequeña
        $this->curPage->setFont($this->fontRegular, self::FONT_SIZE_SMALL);
        
        $string = "Aquesta comanda respon a les necessitats de la unitat de compra pel que fa al material d'oficina";
        $this->drawTextCenter($string, self::MARGIN_LEFT, self::MARGIN_BOTTOM - self::SEP_V - 40, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE_SMALL);
        
        $string = "imprescindile per fer les tasques necessàries en els diversos processos que la unitat ha de dur a terme";
        $this->drawTextCenter($string, self::MARGIN_LEFT, self::MARGIN_BOTTOM - self::SEP_V - 55, $this->pageUsedW, self::SEP_V, $this->fontRegular, self::FONT_SIZE_SMALL);
        */
    }
}