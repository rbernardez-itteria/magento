<?php 

class Itteria_MktpSuperOrder_Model_Ponderacion_Proveedor extends Mage_Core_Model_Abstract {
    
    protected function _construct() {
        parent::_construct();
        $this->_init('mktpsuperorder/ponderacion_proveedor');
    }
      
    protected function _afterSave() {
          
        // Guardamos la info de los items
        $items = $this->getItems();

        foreach ($items as $data) {

            $ponderacion = Mage::getModel('mktpsuperorder/ponderacion')->load($this->getParentId());
            $order = Mage::getModel('sales/order')->load($ponderacion->getOrderId());
            $itemsOrder = $order->getAllItems();
            $data['qty'] = 0;
            foreach($itemsOrder as $itemOrder):
                if($itemOrder->getProductId() == $data['product_id']) {
                    $data['qty'] = $data['qty'] + $itemOrder->getData('qty_ordered');
                }
            endforeach;
            $data['row_total'] = $data['qty'] * $data['price'];
            $item = Mage::getModel('mktpsuperorder/ponderacion_item');
            $item->setData($data);
            $item->setPonderacionId($this->getParentId());
            $item->setProveedorId($this->getId());
            $item->save();
            $data['qty'] = 0;
        }
    }
}
