<?php

class Itteria_MktpSuperOrder_Model_Sales_Order extends Mage_Sales_Model_Order {
    
    public function ponderar($type, $excluded, $guardar, $extra = array()) {
        
        // Preparamos algunos datos
        $unavailable = array();
        if (is_string($excluded)) {
            $excluded = explode(',', $excluded);
        }
        
        // Obtenemos ids de producto y cantidades
        $productCollection[] = array();
        $items = $this->getAllItems();
        foreach($items as $item){
            $productCollection[] = array(
                'id' => $item->getProductId(), 
                'cantidad' => $item->getQtyOrdered()
            );
        }

        // Itteria - Obtenemos el conjunto de websites dadas de alta y las recorremos
        $winner = 1;
        $masbajo = PHP_INT_MAX;
        $websites = Mage::app()->getWebsites();
        foreach ($websites as $website) {
            
            // Saltamos la genérica
            if ($website->getId() == '1') { continue; }
            
            // Iniciamos el total
            $total_tienda = 0;

            // Sumamos los carritos de cada proveedor
            $productos = array();
            foreach ($productCollection as $product) {
                
                // Si no hay id, no podemos usarlo
                if (!isset($product['id'])) { continue; }
                
                // Si el producto no está activo el pedido no se pondera para ese proveedor
                $websiteDisponible = Mage::getModel('catalog/product')->load($product['id'])->getWebsiteIds();
                if(!in_array($website->getId(), $websiteDisponible)){
                    $total_tienda = 0;
                    $unavailable[] = $website->getId();
                    break;
                }
                
                // Cargamos el producto para este store
                $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($product['id']);
                
                // Añadimos a la info de productos
                $pid = $product['id'];
                $productos[$pid] = array(
                    'product_id' => $pid,
                    'qty'        => $product['cantidad'],
                    'price'      => $productWebsite->getPrice(),
                    'row_total'  => $productWebsite->getPrice() * $product['cantidad']
                );

                // Sumamos al total
                if($productWebsite->getPrice() != 0){
                    $total_tienda += $productWebsite->getPrice() * $product['cantidad'];
                }
                else{
                    $total_tienda = 0;
                    break;
                }
            }
            if($total_tienda != 0){
                // Guardamos este proveedor
                $wid = $website->getId();
                $proveedores[$wid] = array(
                    'store_id'  => $website->getId(),
                    'proveedor' => $website->getName(),
                    'subtotal'  => $total_tienda,
                    'items'     => $productos,
                );
                
                // Vemos si es el ganador por el momento
                // No contamos con los ya excluidos
                if ($total_tienda < $masbajo && !in_array($website->getId(), $excluded)) {
                    $winner = $wid;
                    $masbajo = $total_tienda;
                }
            }
        }

        // Creamos y guardamos la ponderación si es necesario
        $ponderacion = Mage::getModel('mktpsuperorder/ponderacion');
        $ponderacion->setOrderId($this->getId());
        $ponderacion->setWinner($winner);
        $ponderacion->setAmount($masbajo);
        $ponderacion->setExcluded(implode(',', $excluded));
        $ponderacion->setUnavailable(implode(',', $unavailable));
        $ponderacion->setType($type);
        $ponderacion->setExtraData(serialize($extra));
        $ponderacion->setProveedores($proveedores);
        
        // Solo se pueden guardar ponderaciones que vayan asociadas a un pedido
        if ($guardar) {
            $ponderacion->save();
        }
        
        return $ponderacion;
    }

    /**
     * Asigna el siguiente proveedor para este pedido
     */
    function siguienteProveedor($type, $comentario) {

        // Itteria - Obtenemos los provedores que ya han sido rechazados para este pedido
        $rechazos = $this->getIdsRechazos();
        
        // Itteria - Introducimos el proveedor que acaba de cancelar el pedido
        $rechazos[] = $this->getStoreId();
        
        // Itteria - Obtenemos el tiempo de respuesta maximo y lo asignamos al pedido
        if ($type == Itteria_MktpSuperOrder_Model_Ponderacion::NUEVO_PEDIDO) {
            $quote = Mage::getSingleton('checkout/session')->getQuote();
            $this->setRespuesta($quote->getRespuesta());
        }

        // Creamos una nueva ponderación del pedido y obtenemos el ganador
        $extra = array();
        if ($type == Itteria_MktpSuperOrder_Model_Ponderacion::RECHAZO) {
            $extra = array(
                'proveedor' => $this->getStoreId(),
                'comentario' => $comentario,
                'total' => $this->getGrandTotal(),
            );
        }
        $ponderacion = $this->ponderar($type, $rechazos, true, $extra);
        $winner = $ponderacion->getWinner();
        
        // Asignamos el nuevo store
        $this->setStoreId($winner);

        // Itteria - Recorremos todos los productos del pedido y modificamos su precio al del nuevo proveedor
        $subtotal = 0;
        foreach($this->getItemsCollection() as $item) {
            // Itteria - Obtenemos el producto del proveedor elegido
            $productWebsite = Mage::getModel('catalog/product')->setStoreId($winner)->load($item->getProduct()->getId());
            // Itteria - Almacenamos su subtotal
            $subtotal = $subtotal + ($item->getQtyOrdered() * $productWebsite->getPrice());
            // Itteria - Establecemos el precio del producto actualizado
            $item->setPrice($productWebsite->getPrice());
            // Itteria - Guardamos los cambios sobre el item del pedido
            $item->save();
        }
        
        // Itteria - Actualizamos el total del pedido
        $tax = Mage::getModel('tax/calculation_rate')->loadByCode('IVA');
        $this->setGrandTotal($subtotal * (($tax['rate']+100)/100));
        
        if ($winner == 1) {
            $this->setStatus("canceled");
            $this->setStoreId(0);
        }
        
        // Comentario en histórico del pedido
        $this->addStatusHistoryComment($comentario);
        
        // Itteria - Actualizamos la fecha
        $this->setCreatedAt(date('d-m-Y H:i:s'));
        
        // Guardamos la comanda
        $this->save();

        // Itteria - Generación de las notificaciones por email
        if(
            (Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_cancel_comanda') && $comentario != 'Nova comanda') ||
            (Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_new_comanda') && $comentario == 'Nova comanda')
        ){

            // Itteria - Elegimos la plantilla que vamos a usar
            if($comentario == 'Nova comanda'){
                $template_encargado_id = 'admin_emails_new_comanda_template';
                $template_proveedor_id = 'admin_emails_new_comanda_proveedor_template';
            }else{
                $template_encargado_id = 'admin_emails_cancel_comanda_template';
                $template_proveedor_id = 'admin_emails_cancel_comanda_proveedor_template';
            }

            // Itteria - Elegimos los destinatarios
            $email_encargado_to = array();
            $email_proveedor_to = array();
            
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            $email_encargado_to[0] = $supervisor->getEmail();
            
            if($comentario == 'Nova comanda'){
                // Itteria - Email encargado material
                $email_encargado_to[1] = $this->getCustomerEmail();
            
                // Itteria - Email nuevo proveedor
                // Itteria - Obtenemos el rol del siguiente proveedor
                $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $winner))->getData();
                // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
                $users = Mage::getModel('admin/user')->getCollection();
                foreach ($users as $user) {
                    if($user->getRole()->getId() == $role[0]['role_id']){
                        $email_proveedor_to[0] = $user->getEmail();
                    }
                }
            }

            // Itteria - Cargamos la plantilla del email
            $email_encargado_template  = Mage::getModel('core/email_template')->loadDefault($template_encargado_id);
            $email_proveedor_template  = Mage::getModel('core/email_template')->loadDefault($template_proveedor_id);

            // Itteria - Preparamos las variables del email
            $store_mail = Mage::getModel('core/store')->load($winner);
            $order_mail = Mage::getModel('sales/order')->load($this->getId());

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'store' => $store_mail,
                'order' => $order_mail,
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_encargado_template->setSenderName($sender_name);
            $email_proveedor_template->setSenderName($sender_name);
            $email_encargado_template->setSenderEmail($sender_email);
            $email_proveedor_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_encargado_to as $_from) {
                $email_encargado_template->send($_from, $_from, $email_template_variables);
            }
            foreach ($email_proveedor_to as $_from) {
                $email_proveedor_template->send($_from, $_from, $email_template_variables);
            }
        }
    }

    /**
     * Creación del informe asociado al pedido
     */
    function crearInforme() {
        
        // Itteria - Creamos una instancia de informe
        $informe = Mage::getModel('mktpsuperorder/informe');
        // Itteria - Asignamos el id del pedido
        $informe->setOrderId($this->getId());
        // Itteria - Asignamos el numero de pedido
        $informe->setOrderCode($this->getIncrementId());
        // Itteria - Asignamos la fecha del pedido
        $informe->setOrderCreatedDate(Mage::getModel('core/date')->date('d-m-Y'));
        // Itteria - Asignamos el estado del pedido
        $informe->setOrderState($this->getStatusLabel());
        // Itteria - Asignamos el encargado de material que realizó el pedido
        $informe->setOrderUser($this->getCustomerName());
        // Itteria - Asignamos el proveedor
        $informe->setOrderProvider(Mage::getModel('core/store')->load($this->getStoreId())->getName());
        // Itteria - Asignamos el total
        $informe->setOrderTotal(round($this->getGrandTotal(),2));

        // Itteria - Guardamos el informe
        $informe->save();


        // Itteria - Almacenamos todos los artículos que hay en el pedido
        foreach ($this->getAllItems() as $item) {

            // Itteria - Obtenemos las opciones personalizadas del producto en el que nos encontramos
            $_customOptions = $item->getProductOptions();
            foreach($_customOptions['options'] as $_option){
                if($_option['label'] == "Adreça d'enviament"){
                    $direccion = $_option['value'];
                }
            }
            $producto = Mage::getModel('catalog/product')->setStoreId($this->getStoreId())->load($item->getProduct()->getId());

            // Itteria - Creamos una instancia de articulo para el informe
            $articulo = Mage::getModel('mktpsuperorder/informe_articulo');

            // Itteria - Establecemos los datos
            $articulo->setOrderId($this->getId());
            $articulo->setInformeId($informe->getId());
            $articulo->setArticleId($producto->getId());
            $articulo->setArticleSku($item->getSku());
            $articulo->setArticleName($item->getName());
            $articulo->setArticleQty(intval($item->getData('qty_ordered')));
            $articulo->setArticleBrand($producto->getData('marca'));
            $articulo->setArticleModel($producto->getData('modelo'));
            $articulo->setArticleProducer($producto->getData('fabricante'));
            $articulo->setArticleCum($producto->getData('unid_venta'));
            $articulo->setArticlePrice($item->getPrice());
            $articulo->setArticleSubtotal($item->getData('qty_ordered')*$item->getPrice());
            $articulo->setArticleAddress($direccion);

            // Itteria - Almacenamos el articulo creado
            $articulo->save();
        }
    }

    /**
     * Actualizamos el informe de un pedido por la cancelación de un proveedor
     */
    function actualizarProveedorInforme($proveedor){

        // Itteria - Obtenemos el informe asociado a la comanda
        $informes = Mage::getModel('mktpsuperorder/informe')->getCollection()->addFieldToFilter('order_id', array('eq' => $this->getId()));
        foreach ($informes as $informe) {
            $informe_actual = Mage::getModel('mktpsuperorder/informe')->load($informe->getData('id'));
        }

        // Itteria - Actualizamos el proveedor
        $informe_actual->setOrderProvider(Mage::getModel('core/store')->load($this->getStoreId())->getName());
        // Itteria - Actualizamos los rechazados
        if($informe_actual->getOrderRejected() != NULL){
            $proveedor = $proveedor . ' - ' . $informe_actual->getOrderRejected();
        }
        $informe_actual->setOrderRejected($proveedor);
        // Itteria - Actualizamos el total
        $informe_actual->setOrderTotal(round($this->getGrandTotal(),2));
        // Itteria - Actualizamos el estado
        $informe_actual->setOrderState($this->getStatusLabel());

        // Itteria - Guardamos los datos del informe
        $informe_actual->save();

        // Itteria - Almacenamos todos los artículos que hay en el pedido
        foreach ($this->getAllItems() as $item) {

            // Itteria - Cargamos el producto asociado al item
            $producto = Mage::getModel('catalog/product')->setStoreId($this->getStoreId())->load($item->getProduct()->getId());
            // Itteria - Obtenemos todos los articulos que coinciden con dicho producto
            $articulos = Mage::getModel('mktpsuperorder/informe_articulo')->getCollection()
                                        ->addFieldToFilter('informe_id', array('eq' => $informe_actual->getId()))
                                        ->addFieldToFilter('article_id', array('eq' => $producto->getId()));

            // Itteria - Actualizamos los valores de los artículos
            foreach ($articulos as $articulo) {
                $articulo->setArticleBrand($producto->getData('marca'));
                $articulo->setArticleModel($producto->getData('modelo'));
                $articulo->setArticleProducer($producto->getData('fabricante'));
                $articulo->setArticleCum($producto->getData('unid_venta'));
                $articulo->setArticlePrice($item->getPrice());
                $articulo->setArticleSubtotal($articulo->getArticleQty()*$item->getPrice());

                $articulo->save();
            }
        }
    }

    /**
     * Actualizamos el informe de un pedido
     */
    function actualizarEstadoInforme($tipo){

        // Itteria - Obtenemos el informe asociado a la comanda
        $informes = Mage::getModel('mktpsuperorder/informe')->getCollection()->addFieldToFilter('order_id', array('eq' => $this->getId()));
        foreach ($informes as $informe) {
            $informe_actual = Mage::getModel('mktpsuperorder/informe')->load($informe->getData('id'));
        }

        // Itteria - Actualizamos el estado
        $informe_actual->setOrderState($this->getStatusLabel());
        
        // Itteria - En función de lo que hagamos establecemos una fecha
        switch ($tipo) {
            case 'aceptada':
                $informe_actual->setOrderAcceptDate(Mage::getModel('core/date')->date('d-m-Y'));
                break;
            case 'rechazada':
                $informe_actual->setOrderRefuseDate(Mage::getModel('core/date')->date('d-m-Y'));
                break;
            case 'confirmada':
                $informe_actual->setOrderConfirmDate(Mage::getModel('core/date')->date('d-m-Y'));
                break;
            case 'enviada':
                $informe_actual->setOrderSentDate(Mage::getModel('core/date')->date('d-m-Y'));
                break;
            case 'recibida':
                $informe_actual->setOrderCompleteDate(Mage::getModel('core/date')->date('d-m-Y'));
                break;
        }
        $informe_actual->save();
    }

    /**
     * Cancelación total de la comanda
     */
    function cancelaComanda($comentario) {
        
        // Itteria - Escribimos un comentario sobre el pedido en el que especificamos que el supervisor ha rechazado el pedido
        $this->addStatusHistoryComment("El administrador ha rebutjat la comanda");
        
        // Itteria - Cerramos el pedido
        $this->setStatus("canceled");

        // Itteria - Generación de las notificaciones por email
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_remove_comanda')){

            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'emails_remove_comanda_template';

            // Itteria - Elegimos los destinatarios
            $email_to = array();
                            
            // Itteria - Email encargado material
            $email_to[0] = $this->getCustomerEmail();
           
            // Itteria - Email nuevo proveedor
            // Itteria - Obtenemos el rol del siguiente proveedor
            $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $this->getStoreId()))->getData();
            // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
            $users = Mage::getModel('admin/user')->getCollection();
            foreach ($users as $user) {
                if($user->getRole()->getId() == $role[0]['role_id']){
                    $email_to[1] = $user->getEmail();
                }
            }

            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'order' => $this,
                'comment' => $comentario,
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_to as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }
 
        // Guardamos la comanda       
        $this->save();
    }

    function getIdsRechazos() {
        return $this->getInfoRechazos(true);
    }

    function getInfoRechazos($onlyIds = false) {
        
        // Obtenemos todos los rechazos para este pedido
        $ponderaciones = Mage::getModel('mktpsuperorder/ponderacion')->getCollection()
                               ->addFieldToSelect('*')
                               ->addFieldToFilter('order_id', $this->getId())
                               ->addFieldToFilter('type', Itteria_MktpSuperOrder_Model_Ponderacion::RECHAZO);

        // Buscamos los IDs de los proveedores que rechazan
        $rechazos = array();
        foreach ($ponderaciones as $ponderacion) {
            $info = $ponderacion->getExtraData();
            $info = unserialize($info);
            if ($onlyIds) {
                $rechazos[] = $info['proveedor'];
            }
            else {
                $rechazos[] = $info;
            }
        }
        
        return $rechazos;
    }

    function getUltimaPonderacion() {
        
        // Obtenemos la última ponderación
        $ponderaciones = Mage::getModel('mktpsuperorder/ponderacion')->getCollection()
                               ->addFieldToSelect('*')
                               ->addFieldToFilter('order_id', $this->getId());
        $ponderaciones->getSelect()->order('id desc');
        
        // Cargamos toda la info para que se carguen proveedores e items
        return Mage::getModel('mktpsuperorder/ponderacion')->load($ponderaciones->getFirstItem()->getId());
    }
}
