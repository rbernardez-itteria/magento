<?php 

  class Itteria_MktpSuperOrder_Model_Budget extends Mage_Core_Model_Abstract {
    
      protected function _construct() {
          parent::_construct();
          $this->_init('mktpsuperorder/budget');
      }    
  }
