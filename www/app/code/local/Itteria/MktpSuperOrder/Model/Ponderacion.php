<?php 

class Itteria_MktpSuperOrder_Model_Ponderacion extends Mage_Core_Model_Abstract {
    
    const NUEVO_PEDIDO = 'new_order';
    const RECHAZO      = 'reject';
    
    protected function _construct() {
        parent::_construct();
        $this->_init('mktpsuperorder/ponderacion');
    }
      
    protected function _afterSave() {

        // Guardamos la info de los proveedores
        $proveedores = $this->getProveedores();
        foreach ($proveedores as $data) {
            $proveedor = Mage::getModel('mktpsuperorder/ponderacion_proveedor');
            $proveedor->setData($data);
            $proveedor->setParentId($this->getId());
            $proveedor->save();
        }
    }
      
    protected function _afterLoad() {

        // Informacion de proveedor
        $info = array();
        
        // Obtenemos la info de los proveedores
        $proveedores = Mage::getModel('mktpsuperorder/ponderacion_proveedor')->getCollection()
                             ->addFieldToFilter('parent_id', $this->getId());
                             
        // Y la añadimos a los datos
        foreach ($proveedores as $proveedor) {

            // Informacion de items
            $itemsinfo = array();
            
            // Obtenemos la info de los items
            $items = Mage::getModel('mktpsuperorder/ponderacion_item')->getCollection()
                           ->addFieldToFilter('ponderacion_id', $proveedor->getParentId())
                           ->addFieldToFilter('proveedor_id', $proveedor->getId());
                                 
            // Y la añadimos a los datos
            foreach ($items as $item) {
                $itemsinfo[] = array(
                    'ponderacion_id' => $proveedor->getParentId(),
                    'proveedor_id' => $proveedor->getId(),
                    'product_id' => $item->getProductId(),
                    'qty' => $item->getQty(),
                    'price' => $item->getPrice(),
                    'row_total' => $item->getRowTotal()
                );
            }
            
            // Info del proveedor
            $info[] = array(
                'store_id'  => $proveedor->getStoreId(),
                'parent_id' => $proveedor->getParentId(),
                'proveedor' => $proveedor->getProveedor(),
                'subtotal'  => $proveedor->getSubtotal(),
                'items'     => $itemsinfo,
            );
        }
        
        $this->setProveedores($info);
    }
}
