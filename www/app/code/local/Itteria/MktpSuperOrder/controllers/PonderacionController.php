<?php

class Itteria_MktpSuperOrder_PonderacionController extends Mage_Sales_Controller_Abstract {
  
    function ponderacionAction(){
        // Itteria - Comprueba el pedido y lo carga
        if (!$this->_loadValidOrder()) {
            return;
        }
        
        // Con esto cargamos el layout del xml
        $this->loadLayout();
        
        // Iniciamos el bloque de mensajes
        $this->_initLayoutMessages('catalog/session');

        // Marcamos en el menú de navegación del usuario la opción activa
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('sales/order/history');
        }
        
        // Render de la página
        $this->renderLayout();
    }
}