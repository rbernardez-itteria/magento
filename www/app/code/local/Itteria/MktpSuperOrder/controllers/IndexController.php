<?php

class Itteria_MktpSuperOrder_IndexController extends Mage_Core_Controller_Front_Action {
  
    function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    function printAction(){
        $this->loadLayout();
        $this->renderLayout();
    }

    function pdfAction() {
        
        // Creamos el pdf
        $pdf = Mage::getModel('mktpsuperorder/pdf')->getPdf($this->getRequest()->getParam('order_id'));
        
        // Lo enviamos por download
        return $this->_prepareDownloadResponse(
                            'pdf'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                            'application/pdf'
                        );    
    }

    function tiempoRespuestaAction(){
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $quote->setRespuesta(Mage::app()->getRequest()->getPost('max-time-order'));
        $quote->save();

        $this->_redirectReferer();
    }
    
    function getItemAddress($item) {

        // Opciones del producto y las reordenamos por id para acceder fácilmente
        $productOptions = array();
        $productOptionsTemp = Mage::getModel('catalog/product')->load($item->getProductId())->getOptions();
        foreach ($productOptionsTemp as $option) {
            $productOptions[$option->getOptionId()] = $option;
        }
        
        // Buscamos entre las opciones del item de wishlist y cruzamos datos con las opciones del producto
        // para obtener el título de la opción y buscar la dirección
        $options = $item->getOptions();
        foreach ($options as $option) {
    
            // donde 1234 es el id de la opción del producto.
            $parts = explode('_', $option->getCode());
            if (count($parts) == 2 && is_numeric($parts[1])) {
    
                // Cargamos la opción del producto
                $option_id = $parts[1];
                $optionInfo = $productOptions[$option_id];
                
                // Si hemos encontrado la dirección, salimos
                if (stripos($optionInfo->getTitle(), 'enviament')) {
                    
                    return $option->getValue();
                }
            }
        }
        
        return 'no hay dirección';
    }
    
    function wishcsvAction(){

        // Itteria - Conseguimos todos los items de la lista
        $_objWishlist = new Itoris_MWishlist_Model_Mwishlistnames();
        $_itemsWish = $_objWishlist->getWishlistItems($this->getRequest()->getParam('wishlist_id'));
        $_collectionItemsWish = array();

        // Itteria - Recorremos todos los items y almacenamos sus ids en un array
        $_countArray = 0;
        foreach ($_itemsWish as $_itemWish) {
            array_push($_collectionItemsWish, $_itemsWish[$_countArray]['id']);
            $_countArray++;
        }

        // Itteria - Obtenemos todos los elementos de la wishlist de Magento
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $_wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer->getId());
        $_items = $_wishlist->getItemCollection();

        // Itteria - Preparamos las columnas del csv para almacenar los datos
        $csv = '';
        $_columns = array(
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Adreça d'enviament"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Producte"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Codi"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Quantitat"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Preu Màxim"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Subtotal"),
        );
        $data = array();
        foreach ($_columns as $column) {
            $data[] = '"'.$column.'"';
        }
        $csv .= implode(';', $data)."\n";

        // Itteria - Recorremos todos los items de la lista de deseos
        foreach ($_items as $_item){
            if(in_array($_item->getId(),$_collectionItemsWish)){

                // Itteria - Obtenemos un listado de todas las websites para obtener el precio máximo del item en el que nos encontramos
                $websites = Mage::app()->getWebsites();
                $precio_maximo = 0;
                foreach ($websites as $website) {
                    $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($_item->getProductId());
                    if($productWebsite->getPrice() > $precio_maximo){
                        $precio_maximo = $productWebsite->getPrice();
                    }
                }

                // Itteria - Almacenamos los datos del pedido en el csv y los separamos por comas
                $data = array();

                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $this->getItemAddress($_item));
                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$productWebsite->getName()));
                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$productWebsite->getData('sku')));
                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", (int)$_item->getQty());
                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format($precio_maximo,2,',','.'));
                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format((int)$_item->getQty()*$precio_maximo,2,',','.'));

                $csv .= implode(';', $data)."\n";
            }
        }

        // Itteria - Preparamos la descarga del fichero
        $nombre_fichero = 'llistes-de-desig-' . date("H:i:s d-m-Y") . '.csv';
        $this->_prepareDownloadResponse($nombre_fichero, $csv, 'text/csv');
    }

    function csvAction(){

        // Itteria - En caso de querer exportar un pedido obtenemos su información
        if($this->getRequest()->getParam('order_id') != NULL){
            $_order = Mage::getModel('sales/order')->load($this->getRequest()->getParam('order_id'));
        }

    	// Itteria - Preparamos las columnas del csv para almacenar los datos
    	$csv = '';
    	$_columns = array(
    	    iconv("UTF-8", "ISO-8859-1//TRANSLIT","Adreça d'enviament"),
    	    iconv("UTF-8", "ISO-8859-1//TRANSLIT","Producte"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Codi"),
    	    iconv("UTF-8", "ISO-8859-1//TRANSLIT","Quantitat"),
    	    iconv("UTF-8", "ISO-8859-1//TRANSLIT","Preu Màxim"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Subtotal"),
    	);
    	$data = array();
    	foreach ($_columns as $column) {
    	    $data[] = '"'.$column.'"';
    	}
    	$csv .= implode(';', $data)."\n";

        // Itteria - Obtenemos el carrito actual o el pedido al que queremos exportar
        if($_order != NULL){
            $cart = $_order;
        }else{
            $cart = Mage::getModel('checkout/cart')->getQuote();
        }
        

    	// Itteria: Creamos un array para introducir todas las direcciones diferentes que hay en los productos que queremos comprar
    	$direcciones = array();
    	// Itteria: Recorremos todos los productos del carrito y vamos almacenando todas las direcciones diferentes a las que se quiere enviar
    	foreach($cart->getAllItems() as $_item){

            // Itteria - Dependiendo de si es un pedido o un carrito obtenemos sus opciones personalizadas
            if($_order != NULL){
                $_customOptions = $_item->getProductOptions();
            }else{
                $_customOptions = $_item->getProduct()->getTypeInstance(true)->getOrderOptions($_item->getProduct());
            }
    	    
    	    foreach($_customOptions['options'] as $_option){
    	        if($_option['label'] == "Adreça d'enviament"){
    	            if(!in_array($_option['value'], $direcciones)){
    	                array_push($direcciones, $_option['value']);
    	            }
    	        }
    	    }
    	} 

    	// Itteria: Recorremos todas las direcciones diferentes que hay para los productos
    	foreach ($direcciones as $direccion){

            // Itteria: Recorremos todos los productos que hay en el carro
    		foreach($cart->getAllItems() as $_item){

                // Itteria: Obtenemos todas las opciones personalizadas del producto en el que nos encontramos
                if($_order != NULL){
                    $_customOptions = $_item->getProductOptions();
                }else{
                    $_customOptions = $_item->getProduct()->getTypeInstance(true)->getOrderOptions($_item->getProduct());
                }
                
                // Itteria: Recorremos todas las opciones para encontrar el apartado donde define la dirección
                foreach($_customOptions['options'] as $_option){
                    if($_option['label'] == "Adreça d'enviament"){

                        // Itteria: Solo entramos si el producto corresponde con la dirección en la que estamos
                        if($direccion == $_option['value']){
                            $producto = $_item->getProduct();

                            if($_order == NULL){
                                $websites = Mage::app()->getWebsites();
                                $precio_maximo = 0;
                                foreach ($websites as $website){
                                    $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($producto->getId());
                                    if($productWebsite->getPrice() > $precio_maximo){
                                        $precio_maximo = $productWebsite->getPrice();
                                    }
                                }
                            }

                            // Itteria - Almacenamos los datos del pedido en el csv y los separamos por comas
                            $data = array();
                            
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $direccion);
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$producto->getName()));
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$producto->getData('sku')));
                            
                            if($_order != NULL){  
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", intval($_item->getData('qty_ordered')));
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format($_item->getPrice(),2,',','.'));
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format(($_item->getPrice()*intval($_item->getData('qty_ordered'))),2,',','.'));
                            }else{
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $_item->getQty());
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format($precio_maximo,2,',','.'));
                                $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format($precio_maximo*$_item->getQty(),2,',','.'));
                            }
                            $csv .= implode(';', $data)."\n";
                        }
                    }
                }
            }
    	}
        // Itteria - Preparamos la descarga del fichero
        $nombre_fichero = 'comanda-' . date("H:i:s d-m-Y") . '.csv';
        $this->_prepareDownloadResponse($nombre_fichero, $csv, 'text/csv');
    }
}
