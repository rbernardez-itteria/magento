<?php

class Itteria_MktpSuperOrder_Adminhtml_MktpSuperOrder_ComandasController extends Mage_Adminhtml_Controller_Action {
    
    function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    // Itteria - Exportaciones de los informes de peticiones
    public function exportCsvAction() {
        $fileName   = 'csv-comandes-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.csv';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_comandas_listadoComandas_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelAction() {
        $fileName   = 'xml-comandes-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.xml';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_comandas_listadoComandas_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function gridAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('mktpsuperorder/adminhtml_comandas_listadoComandas_grid')->toHtml()
        );
    }
}