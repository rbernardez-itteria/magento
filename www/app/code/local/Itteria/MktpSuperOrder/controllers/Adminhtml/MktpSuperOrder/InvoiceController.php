<?php

// Itteria - Este controlador se encarga de aceptar el pedido automaticamente evitandonos una pantalla de confirmación del pedido
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'OrderController.php';
class Itteria_MktpSuperOrder_Adminhtml_MktpSuperOrder_InvoiceController extends Mage_Adminhtml_Sales_OrderController {
    
    function indexAction() {
        
        $order = Mage::getModel('sales/order')->load($this->getRequest()->getParam('order_id'));
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
        $invoice->register();

        $invoice->getOrder()->setCustomerNoteNotify(false);          
        $invoice->getOrder()->setIsInProcess(true);

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder());

        $transactionSave->save();

        $order->addStatusHistoryComment('El día ' . date("d-m-Y H:i:s") .' el proveïdor ' . $order->getStore()->getName() . ' va acceptar la comanda');
        $order->save();

        // Itteria - Actualizamos el informe asociado a la comanda
        $order->actualizarEstadoInforme('aceptada');

        // Itteria - Envío de los emails
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_accept_comanda')){

            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'admin_emails_accept_comanda_template';

            // Itteria - Elegimos los destinatarios
            $email_to = array();
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            $email_to[0] = $supervisor->getEmail();
            // Itteria - Email encargado material
            $email_to[1] = $order->getCustomerEmail();

            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'order' => $order,
                'customer' => $customer,
                'supervisor' => $supervisor->getEmail(),
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_to as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }

        $this->_redirectReferer();
    }
}