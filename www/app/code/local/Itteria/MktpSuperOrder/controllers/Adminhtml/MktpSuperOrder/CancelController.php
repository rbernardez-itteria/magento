<?php
  
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'OrderController.php';
class Itteria_MktpSuperOrder_Adminhtml_MktpSuperOrder_CancelController extends Mage_Adminhtml_Sales_OrderController {
    
    function indexAction() {

        $this->_title($this->__('Sales'))->_title($this->__('Orders'));

        $order = $this->_initOrder();
        if ($order) {

            $isActionsNotPermitted = $order->getActionFlag(
                Mage_Sales_Model_Order::ACTION_FLAG_PRODUCTS_PERMISSION_DENIED
            );
            if ($isActionsNotPermitted) {
                $this->_getSession()->addError($this->__('You don\'t have permissions to manage this order because of one or more products are not permitted for your website.'));
            }

            $this->_initAction();

            $this->_title(sprintf("#%s", $order->getRealOrderId()));

            $this->renderLayout();
        }
    }

    function saveAction(){

        // Itteria - Cargamos el pedido que queremos cancelar
        $_order = Mage::getModel('sales/order')->load($this->getRequest()->getPost('order_id'));

        // Itteria - Obtenemos el comentario que ha escrito
        $_comentario = $this->getRequest()->getPost('comentario_cancelacion');

        //Itteria - Comprobamos el tipo de usuario que está conectado
        $roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
        $roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();

        if(strtolower($roleName) != 'supervisor'){
            // Siguiente proveedor
            $comment = "El proveïdor " . $_order->getStore()->getName() . " no ha acceptat la comanda, ha donat la següent raó: " . $_comentario . ". Preu Comanda: " . number_format($_order->getGrandTotal(),2,',','.') . ' €'; 
            $proveedor = Mage::getModel('core/store')->load($_order->getStoreId())->getName();
            $_order->siguienteProveedor(Itteria_MktpSuperOrder_Model_Ponderacion::RECHAZO, $comment);
            $_order->actualizarProveedorInforme($proveedor);
            
        }else{
            
            // Cancela la comanda
            $_order->cancelaComanda($_comentario);
            $_order->actualizarEstadoInforme('rechazada');
        }

        // Itteria - Redirigimos a la ficha del pedido
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("*/sales_order/view", array('order_id'=> $this->getRequest()->getPost('order_id'))))->sendResponse();
    }
}