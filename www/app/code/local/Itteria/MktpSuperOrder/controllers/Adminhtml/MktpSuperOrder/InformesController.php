<?php

class Itteria_MktpSuperOrder_Adminhtml_MktpSuperOrder_InformesController extends Mage_Adminhtml_Controller_Action {
    
    function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    function articuloPeticionAction() {
    	$this->loadLayout();
    	$this->renderLayout();
    }

    function informeArticuloAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    function peticionArticuloAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /*
    *   LISTADO PETICIONES
    *
    */
    public function gridListadoPeticionesAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_grid')->toHtml()
        );
    }

    public function gridListadoPeticionesArticuloAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_Articulo_grid')->toHtml()
        );
    }

    public function gridListadoArticulosAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_grid')->toHtml()
        );
    }

    public function gridListadoArticulosPeticionAction(){
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_Peticion_grid')->toHtml()
        );
    }


    /*
    *   EXPORTACIONES
    *
    */

    // Itteria - Exportaciones de los informes de peticiones
    public function exportCsvListadoPeticionesAction() {
        $fileName   = 'csv-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.csv';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelListadoPeticionesAction() {
        $fileName   = 'xml-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.xml';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function exportpdfListadoPeticionesAction() {

        // Itteria - Obtenemos el archivo CSV
        $grid = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_grid');
        $csv = $grid->getCsvFile();
        
        // Itteria - Extraemos los datos del fichero CSV y lo mandamos al PDF
        $ubicacion = str_replace("export\\", "export", $csv['value']); 
        $row = 0;
        $datos = array();
        if (($handle = fopen($ubicacion, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row != 0){
                    $num = count($data);
                    for ($c=0; $c < $num; $c++) {
                        if($c != 0 && $c != 4 && $c != 11){
                            $datos[$row][] = $data[$c];
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        
        // Creamos el pdf
        $pdf = Mage::getModel('mktpsuperorder/informe_listadoPeticiones_pdf')->getPdf($datos);
        
        // Lo enviamos por download
        return $this->_prepareDownloadResponse(
            'pdf-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
            'application/pdf'
        );    
    }

    // Itteria - Exportaciones de los articulos de una peticion
    public function exportCsvListadoPeticionesArticuloAction() {
        $fileName   = 'csv-articles-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.csv';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_articulo_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelListadoPeticionesArticuloAction() {
        $fileName   = 'xml-articles-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.xml';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_articulo_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function exportpdfListadoPeticionesArticuloAction() {

        // Itteria - Obtenemos el archivo CSV
        $grid = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoPeticiones_articulo_grid');
        $csv = $grid->getCsvFile();
        
        // Itteria - Extraemos los datos del fichero CSV y lo mandamos al PDF
        $ubicacion = str_replace("export\\", "export", $csv['value']); 
        $row = 0;
        $datos = array();
        if (($handle = fopen($ubicacion, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row != 0){
                    $num = count($data);
                    for ($c=0; $c < $num; $c++) {
                        //if($c != 0 && $c != 4){
                            $datos[$row][] = $data[$c];
                        //}
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        
        // Creamos el pdf
        $pdf = Mage::getModel('mktpsuperorder/informe_listadoPeticiones_articulo_pdf')->getPdf($datos);
        
        // Lo enviamos por download
        return $this->_prepareDownloadResponse(
            'pdf-articles-peticions-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
            'application/pdf'
        );    
    }



    // Itteria - Exportaciones de los informes de articulos
    public function exportCsvListadoArticulosAction() {
        $fileName   = 'csv-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.csv';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelListadoArticulosAction() {
        $fileName   = 'xml-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.xml';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    function exportpdfListadoArticulosAction() {
        
        // Itteria - Obtenemos el archivo CSV
        $grid = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_grid');
        $csv = $grid->getCsvFile();
        
        // Itteria - Extraemos los datos del fichero CSV y lo mandamos al PDF
        $ubicacion = str_replace("export\\", "export", $csv['value']); 
        $row = 0;
        $datos = array();
        if (($handle = fopen($ubicacion, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row != 0){
                    $num = count($data);
                    for ($c=0; $c < $num; $c++) {
                        //if($c != 0 && $c != 4){
                            $datos[$row][] = $data[$c];
                        //}
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        
        // Creamos el pdf
        $pdf = Mage::getModel('mktpsuperorder/informe_listadoArticulos_pdf')->getPdf($datos);
        
        // Lo enviamos por download
        return $this->_prepareDownloadResponse(
            'pdf-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
            'application/pdf'
        );   
    }

    // Itteria - Exportaciones de las peticiones de un articulo
    public function exportCsvListadoArticulosPeticionAction() {
        $fileName   = 'csv-peticions-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.csv';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_peticion_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelListadoArticulosPeticionAction() {
        $fileName   = 'xml-peticions-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.xml';
        $grid       = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_peticion_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    function exportpdfListadoArticulosPeticionAction() {
        
        // Itteria - Obtenemos el archivo CSV
        $grid = $this->getLayout()->createBlock('mktpsuperorder/adminhtml_informes_listadoArticulos_peticion_grid');
        $csv = $grid->getCsvFile();
        
        // Itteria - Extraemos los datos del fichero CSV y lo mandamos al PDF
        $ubicacion = str_replace("export\\", "export", $csv['value']); 
        $row = 0;
        $datos = array();
        if (($handle = fopen($ubicacion, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row != 0){
                    $num = count($data);
                    for ($c=0; $c < $num; $c++) {
                        if($c != 0 && $c != 4 && $c != 11){
                            $datos[$row][] = $data[$c];
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        
        // Creamos el pdf
        $pdf = Mage::getModel('mktpsuperorder/informe_listadoArticulos_peticion_pdf')->getPdf($datos);
        
        // Lo enviamos por download
        return $this->_prepareDownloadResponse(
            'pdf-peticions-articles-'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
            'application/pdf'
        );    
    }
}