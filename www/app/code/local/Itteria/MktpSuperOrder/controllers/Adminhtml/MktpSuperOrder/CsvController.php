<?php

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'OrderController.php';
class Itteria_MktpSuperOrder_Adminhtml_MktpSuperOrder_CsvController extends Mage_Adminhtml_Sales_OrderController {
    
    function indexAction() {
        
        $_order = Mage::getModel('sales/order')->load($this->getRequest()->getParam('order_id'));

        
        // Itteria - Preparamos las columnas del csv para almacenar los datos
        $csv = '';
        $_columns = array(
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Adreça d'enviament"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Producte"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Codi"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Quantitat"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Preu"),
            iconv("UTF-8", "ISO-8859-1//TRANSLIT","Total")
        );
        $data = array();
        foreach ($_columns as $column) {
            $data[] = '"'.$column.'"';
        }
        $csv .= implode(';', $data)."\n";

        // Itteria: Creamos un array para introducir todas las direcciones diferentes que hay en los productos que queremos comprar
        $direcciones = array();
        // Itteria: Recorremos todos los productos del carrito y vamos almacenando todas las direcciones diferentes a las que se quiere enviar
        foreach($_order->getAllItems() as $_item){
            $_customOptions = $_item->getProductOptions();
            foreach($_customOptions['options'] as $_option){
                if($_option['label'] == "Adreça d'enviament"){
                    if(!in_array($_option['value'], $direcciones)){
                        array_push($direcciones, $_option['value']);
                    }
                }
            }
        }

        // Itteria: Recorremos todas las direcciones diferentes que hay para los productos
        foreach ($direcciones as $direccion){

            // Itteria: Recorremos todos los productos que hay en el carro
            foreach($_order->getAllItems() as $_item){

                // Itteria: Obtenemos todas las opciones personalizadas del producto en el que nos encontramos
                $_customOptions = $_item->getProductOptions();
                
                // Itteria: Recorremos todas las opciones para encontrar el apartado donde define la dirección
                foreach($_customOptions['options'] as $_option){
                    if($_option['label'] == "Adreça d'enviament"){

                        // Itteria: Solo entramos si el producto corresponde con la dirección en la que estamos
                        if($direccion == $_option['value']){
                            $producto = $_item->getProduct();

                            // Itteria - Almacenamos los datos del pedido en el csv y los separamos por comas
                            $data = array();
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $direccion);
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$producto->getName()));
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", str_replace(',','',$producto->getData('sku')));
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", intval($_item->getData('qty_ordered')));
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format($_item->getPrice(),2,',','.'));
                            $data[] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", number_format(($_item->getPrice()*intval($_item->getData('qty_ordered'))),2,',','.'));

                            $csv .= implode(';', $data)."\n";
                        }
                    }
                }
            }
        }
        // Itteria - Preparamos la descarga del fichero
        $nombre_fichero = 'comanda-' . date("H:i:s d-m-Y") . '.csv';
        $this->_prepareDownloadResponse($nombre_fichero, $csv, 'text/csv');
    }
}