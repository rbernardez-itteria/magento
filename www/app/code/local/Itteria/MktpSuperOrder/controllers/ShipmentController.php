<?php

class Itteria_MktpSuperOrder_ShipmentController extends Mage_Core_Controller_Front_Action {
  
    function acceptAction() {      
        $id = $this->getRequest()->getPost('shipment_id');
        $shipment = Mage::getModel('sales/order_shipment')->load($id);
        $shipment->setAceptado('Enviado');
        $shipment->save();
        
        Mage::getSingleton('core/session')->addSuccess('Envío aceptado correctamente'); 
        $this->_redirectReferer();
    }

    function acceptShipAction(){

        // Itteria - Obtenemos el pedido sobre el que estamos editando los envios
        $order = Mage::getModel('sales/order')->load($this->getRequest()->getPost('order_id'));
        $envio_id = $this->getRequest()->getPost('shipment_id');
        $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
        
        // Itteria - Almacenamos todos los checkbox que se han marcado
        $checkbox_id = array();
        // Itteria - Almacenamos todas las lineas de producto del envío
        $lineaproducto_id = array();
        // Itteria - Recorremos toda la respuesta del POST y vamos almacenando en el array correspondiente
        foreach($this->getRequest()->getPost('check') as $check) {
            if(strpos($check, 'empty')){
                array_push($lineaproducto_id, intval(filter_var($check, FILTER_SANITIZE_NUMBER_INT)));
            }else{
                array_push($checkbox_id, intval(filter_var($check, FILTER_SANITIZE_NUMBER_INT)));
            }
        }

        // Itteria - Recorremos todo el array
        foreach ($lineaproducto_id as $shipment_id) {
            // Itteria - Cargamos la línea de envío
            $shipment = Mage::getModel('sales/order_shipment_item')->load($shipment_id);

            // Itteria - Si la linea de envio está pendiente y el checkbox ha sido marcado cambiamos el estado
            if(in_array($shipment_id, $checkbox_id) && $shipment->getAceptado() == 'Pendent'){
                $shipment->setAceptado('Rebut');
                $shipment->save();
            }
            // Itteria - Si la linea de envio está enviada pero el checkbox ha sido desmarcado cambiamos el estado
            if(!in_array($shipment_id, $checkbox_id) && $shipment->getAceptado() == 'Rebut'){
                $shipment->setAceptado('Pendent');
                $shipment->save();
            }
        }
        
        // Itteria - Generación de las notificaciones por email
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_accept_shipment')){

            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'emails_accept_shipment_template';

            // Itteria - Elegimos los destinatarios
            $email_to = array();
            
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            $email_to[0] = $supervisor->getEmail();
            
            // Itteria - Email proveedor
            // Itteria - Obtenemos el rol del proveedor
            $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $order->getStoreId()))->getData();

            // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
            $users = Mage::getModel('admin/user')->getCollection();
            foreach ($users as $user) {
                if($user->getRole()->getId() == $role[0]['role_id']){
                    $email_to[1] = $user->getEmail();
                    $usuario = $user;
                }
            }
            
            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'usuario' => $user,
                'order' => $order,
                'supervisor' => $supervisor,
                'shipment' => $envio_id,
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_to as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }

        $this->_redirectReferer(); 
    }
  
    function addCommentAction() {
        // Itteria: Creamos una instancia del modelo
        $envio = Mage::getModel('sales/order_shipment_comment');
        // Itteria: Establecemos los campos que hay en base de datos
        $id = $this->getRequest()->getPost('shipment_id');
        $envio->setParentId($id);
        $envio->setCustomerNotified(true);
        $envio->setIsVisibleOnFront(true);
        $envio->setComment($this->getRequest()->getPost('comment'));
        // Itteria: Guardamos la nueva instancia
        $envio->save();

        // Itteria - Obtenemos el pedido
        $order = Mage::getModel('sales/order')->load($this->getRequest()->getPost('order_id'));
        $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');

        // Itteria - Generación de las notificaciones por email
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_add_comment')){

            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'emails_add_comment_template';

            // Itteria - Elegimos los destinatarios
            $email_to = array();
            
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            $email_to[0] = $supervisor->getEmail();
            
            // Itteria - Email proveedor
            // Itteria - Obtenemos el rol del proveedor
            $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $order->getStoreId()))->getData();

            // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
            $users = Mage::getModel('admin/user')->getCollection();
            foreach ($users as $user) {
                if($user->getRole()->getId() == $role[0]['role_id']){
                    $email_to[1] = $user->getEmail();
                    $usuario = $user;
                }
            }
            
            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'comment' => $this->getRequest()->getPost('comment'),
                'order' => $order,
                'envio' => $envio->getIncrementId(),
                'usuario' => $usuario,
                'supervisor' => $supervisor
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_to as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }

        // Itteria: Creamos un comentario y redirigimos
        Mage::getSingleton('core/session')->addSuccess('Comentari enviat correctament');
        $this->_redirectReferer();
    }
}
