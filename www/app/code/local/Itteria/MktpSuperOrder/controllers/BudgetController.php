<?php

class Itteria_MktpSuperOrder_BudgetController extends Mage_Sales_Controller_Abstract {
  
    function acceptAction() {
    	// Itteria - Creamos un array donde guardaremos todos los datos que obtenemos del formulario
    	$aceptacion_presupuesto = array();

        $aceptacion_presupuesto['order_id'] = $this->getRequest()->getPost('order_id');
    	$aceptacion_presupuesto['expediente_cod'] = $this->getRequest()->getPost('expediente_cod');
    	$aceptacion_presupuesto['oficina_cod'] = $this->getRequest()->getPost('oficina_cod');
    	$aceptacion_presupuesto['organo_cod'] = $this->getRequest()->getPost('organo_cod');
    	$aceptacion_presupuesto['unidad_cod'] = $this->getRequest()->getPost('unidad_cod');
    	$aceptacion_presupuesto['facturacion_dir'] = $this->getRequest()->getPost('facturacion_dir');

        // Itteria - Cargamos la instancia del pedido asociado
        $order = Mage::getModel('sales/order')->load($this->getRequest()->getPost('order_id'));

        // Itteria - Obtenemos una instancia de presupuesto
        if($order->getPresupuesto() == "Pendiente"){
            $presupuesto = Mage::getModel('mktpsuperorder/budget');
        }else{
            $presupuesto = Mage::getModel('mktpsuperorder/budget')->load($order->getPresupuesto());
        }

    	// Itteria - Guardamos los datos que hemos obtenido
        $presupuesto->setOrderId($aceptacion_presupuesto['order_id']);
    	$presupuesto->setExpediente($aceptacion_presupuesto['expediente_cod']);
    	$presupuesto->setOficina($aceptacion_presupuesto['oficina_cod']);
    	$presupuesto->setOrgano($aceptacion_presupuesto['organo_cod']);
    	$presupuesto->setUnidad($aceptacion_presupuesto['unidad_cod']);
    	$presupuesto->setFacturacion($aceptacion_presupuesto['facturacion_dir']);

    	// Itteria - Guardamos la instancia en la base de datos
    	$presupuesto->save();

    	// Itteria - Le asignamos el presupuesto
    	$order->setPresupuesto($presupuesto->getId());

    	// Itteria - Guardamos los cambios sobre el pedido 
    	$order->save();

    	// Itteria - Mostramos la confirmación y redirijimos
    	Mage::getSingleton('core/session')->addSuccess('Dades econòmiques incorporades correctament'); 
    	$this->_redirectReferer();
    }

    function confirmAction() {
        
        // Itteria - Cargamos el pedido
        $order = Mage::getModel('sales/order')->load($this->getRequest()->getPost('order_id'));
        $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');

        // Itteria - Cambiamos el estado del pedido
        $order->setStatus("processing");

        // Itteria - Guardamos los cambios sobre el pedido 
        $order->save();

        // Itteria - Actualizamos el estado del informe
        $order->actualizarEstadoInforme('confirmada');

        // Itteria - Generación de las notificaciones por email
        if(Mage::getStoreConfig('mktpsuperorder2/itteria_emails/mail_confirm_comanda')){

            // Itteria - Elegimos la plantilla que vamos a usar
            $template_id = 'emails_confirm_comanda_template';

            // Itteria - Elegimos los destinatarios
            $email_to = array();
            
            // Itteria - Email supervisor
            $supervisor = Mage::getModel('admin/user')->loadByUsername('supervisor');
            $email_to[0] = $supervisor->getEmail();
            
            // Itteria - Email proveedor
            // Itteria - Obtenemos el rol del siguiente proveedor
            $role = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', array('eq' => $order->getStoreId()))->getData();

            // Itteria - De entre todos los usuarios buscamos el siguiente proveedor y obtenemos su email
            $users = Mage::getModel('admin/user')->getCollection();
            foreach ($users as $user) {
                if($user->getRole()->getId() == $role[0]['role_id']){
                    $email_to[1] = $user->getEmail();
                }
            }
            
            // Itteria - Cargamos la plantilla del email
            $email_template  = Mage::getModel('core/email_template')->loadDefault($template_id);

            // Itteria - Cargamos las variables que nos van a hacer falta en el email
            $email_template_variables = array(
                'order' => $order,
                'customer' => $customer,
                'supervisor' => $supervisor->getEmail(),
            );

            // Itteria - Configuramos el resto de ajustes del mail
            $sender_name = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
            $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
            $email_template->setSenderName($sender_name);
            $email_template->setSenderEmail($sender_email); 

            // Itteria - Enviamos los emails a los destinatarios
            foreach ($email_to as $_from) {
                $email_template->send($_from, $_from, $email_template_variables);
            }
        }

        // Itteria - Mostramos la confirmación y redirijimos
        Mage::getSingleton('core/session')->addSuccess("S'ha confirmat la comanda correctament"); 
        $this->_redirectReferer();
    }

    function budgetAction(){
        
        // Itteria - Comprueba el pedido y lo carga
        if (!$this->_loadValidOrder()) {
            return;
        }
        
        // Con esto cargamos el layout del xml
        $this->loadLayout();
        
        // Iniciamos el bloque de mensajes
        $this->_initLayoutMessages('catalog/session');

        // Marcamos en el menú de navegación del usuario la opción activa
        $navigationBlock = $this->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('sales/order/history');
        }
        
        // Render de la página
        $this->renderLayout();
    }
}