<?php

  class Itteria_CentroCoste_Adminhtml_CentroCoste_IndexController extends Mage_Adminhtml_Controller_Action {
    
    function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    function editAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('centrocoste/adminhtml_centroCoste_edit'));
        $this->_addLeft($this->getLayout()->createBlock('centrocoste/adminhtml_centroCoste_edit_tabs'));
        $this->renderLayout();
    }

    function newAction() {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('centrocoste/adminhtml_centroCoste_new'));
        $this->_addLeft($this->getLayout()->createBlock('centrocoste/adminhtml_centroCoste_new_tabs'));
        $this->renderLayout();
    }

    function saveAction() {

      // Tipo de envío
      $type = $this->getRequest()->getPost('form_type');
      
      if ($type == 'new') {
        
        // Creamos el centro y guardamos
        $centro = Mage::getModel('centrocoste/centrocoste');
        $centro->setReference($this->getRequest()->getPost('reference'));
        $centro->setName($this->getRequest()->getPost('name'));
        $centro->setComments($this->getRequest()->getPost('comments'));
        $centro->save();
       
        // Mostramos exito
        Mage::getSingleton('adminhtml/session')->addSuccess('Centre de cost creat');
      }
      else {
        
        // Cargamos la cuenta
        $centro = Mage::getModel('centrocoste/centrocoste')->load($this->getRequest()->getPost('id'));
        $centro->setReference($this->getRequest()->getPost('reference'));
        $centro->setName($this->getRequest()->getPost('name'));
        $centro->setComments($this->getRequest()->getPost('comments'));
        $centro->save();

        // Mostramos exito
        Mage::getSingleton('adminhtml/session')->addSuccess('Centre de cost modificat');
      }

      // Vamos al detalle de la cuenta
      Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/*/edit', array('id' => $centro->getId())));      
    }
  }
