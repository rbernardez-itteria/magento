<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
              'customer', 
              'centrocoste', 
                  array(
                      'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                      'type'              => 'int',
                      'label'             => 'Centro Coste',
                      'input'             => 'select',
                      'source'            => 'centrocoste/entity_attribute_source_type_centroCoste',
                      'backend'           => '',
                      'frontend'          => '',
                      'required'          => true,
                      'visible_on_front'  => false,
                      'sort_order'        => 5,
                  )
);

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "centrocoste");
        
$used_in_forms=array();
$used_in_forms[]="adminhtml_customer";

$attribute->setData("used_in_forms", $used_in_forms)
          ->setData("is_used_for_customer_segment", true)
          ->setData("is_system", 0)
          ->setData("is_user_defined", 1)
          ->setData("is_visible", 1)
          ->setData("sort_order", 100);

$attribute->save();
        
$installer->endSetup();