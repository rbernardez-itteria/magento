<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('centrocoste/centrocoste')};
    CREATE TABLE {$this->getTable('centrocoste/centrocoste')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `reference` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `comments` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$installer->endSetup();
