<?php

    class Itteria_CentroCoste_Model_Entity_Attribute_Source_Type_CentroCoste extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {
        
        var $_options;
        
        public function getAllOptions() {

            if (is_null($this->_options)) {
              
                $centros = Mage::getModel('centrocoste/centrocoste')->getCollection();
                foreach ($centros as $centro) {
                    $this->_options[] = array(
                        'label' => $centro->getReference() . ' / ' . $centro->getName(),
                        'value' => $centro->getId()
                    );
                }
            }
            return $this->_options;
        }
     
        public function toOptionArray() {
            return $this->getAllOptions();
        }
        
    }