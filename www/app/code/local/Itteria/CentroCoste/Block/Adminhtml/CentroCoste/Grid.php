<?php

  class Itteria_CentroCoste_Block_Adminhtml_CentroCoste_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    
      public function __construct() {
          parent::__construct();

          $this->setDefaultSort('id');
          $this->setId('centrocoste_centrocoste_grid');
          $this->setDefaultDir('asc');
          $this->setSaveParametersInSession(true);
      }
       
      protected function _getCollectionClass() {
          return 'centrocoste/centrocoste_collection';
      }
       
      protected function _prepareCollection()
      {
          $collection = Mage::getResourceModel($this->_getCollectionClass());
          $this->setCollection($collection);
          
          $c = parent::_prepareCollection();
          return $c;
      }
       
      protected function _prepareColumns() {
        
          $this->addColumn('reference',
              array(
                  'header'=> $this->__('Referència'),
                  'align' =>'left',
                  'index' => 'reference'
              )
          );
        
          $this->addColumn('name',
              array(
                  'header'=> $this->__('Nom'),
                  'align' =>'left',
                  'index' => 'name'
              )
          );
        
          $this->addColumn('comments',
              array(
                  'header'=> $this->__('Comentaris'),
                  'align' =>'left',
                  'index' => 'comments'
              )
          );

          return parent::_prepareColumns();
      }
       
      public function getRowUrl($row) {

          return $this->getUrl('*/*/edit', array('id' => $row->getId()));
      }
  }
