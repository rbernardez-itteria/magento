<?php

  class Itteria_CentroCoste_Block_Adminhtml_CentroCoste_Edit_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form {
    
      protected function _prepareForm()
      {
          $form = new Varien_Data_Form();
          $this->setForm($form);
          
          $fieldset = $form->addFieldset('form_detail', array('legend'=>Mage::helper('centrocoste')->__('Dades')));
          
          $centro = Mage::getModel('centrocoste/centrocoste')->load($this->getRequest()->getParam('id'));
        
          $fieldset->addField('form_type', 'hidden', array(
            'name'      => 'id',
            'value'     => $centro->getId()
          ));
          
          $fieldset->addField('reference', 'text', array(
            'label'     => Mage::helper('centrocoste')->__('Referència'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'reference',
            'value'     => $centro->getReference()
          ));
          
          $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('centrocoste')->__('Nom'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
            'value'     => $centro->getName()
          ));
          
          $fieldset->addField('comments', 'text', array(
            'label'     => Mage::helper('centrocoste')->__('Comentaris'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'comments',
            'value'     => $centro->getComments()
          ));
  
          return parent::_prepareForm();
      }
  }