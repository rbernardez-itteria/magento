<?php

  class Itteria_CentroCoste_Block_Adminhtml_CentroCoste extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
      
        $this->_blockGroup = 'centrocoste';
        $this->_controller = 'adminhtml_centroCoste';
        $this->_headerText = $this->__('Centres de cost');
        parent::__construct();
    }
  }