<?php

class Itteria_CentroCoste_Block_Adminhtml_CentroCoste_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'centrocoste';
        $this->_controller = 'adminhtml_centroCoste';

        $this->_removeButton('reset');
        $this->_removeButton('delete');
    }
 
    public function getHeaderText()
    {
        return Mage::helper('centrocoste')->__('Nou centre de cost');
    }
}
