<?php

class Itteria_CentroCoste_Block_Adminhtml_CentroCoste_New_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        $fieldset = $form->addFieldset('form_detail', array('legend'=>Mage::helper('centrocoste')->__('Dades')));
        
        $cuenta = Mage::getModel('centrocoste/centrocoste')->load($this->getRequest()->getParam('id'));
        
        $fieldset->addField('form_type', 'hidden', array(
          'name'      => 'form_type',
          'value'     => 'new'
        ));
        
        $fieldset->addField('reference', 'text', array(
            'label'     => Mage::helper('centrocoste')->__('Referència'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'reference',
            'value'     => ''
        ));
        
        $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('centrocoste')->__('Nom'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
          'value'     => ''
        ));
        
        $fieldset->addField('comments', 'text', array(
          'label'     => Mage::helper('centrocoste')->__('Comentaris'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'comments',
          'value'     => ''
        ));
          
        return parent::_prepareForm();
    }
}