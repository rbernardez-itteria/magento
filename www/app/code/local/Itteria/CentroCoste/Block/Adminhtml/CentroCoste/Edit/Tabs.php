<?php

  class Itteria_CentroCoste_Block_Adminhtml_CentroCoste_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('centrocoste')->__('Centre de cost'));
    }
   
    protected function _beforeToHtml()
    {
        $this->addTab('form_detail', array(
            'label'     => Mage::helper('centrocoste')->__('Dades del centre de cost'),
            'title'     => Mage::helper('centrocoste')->__('Dades del centre de cost'),
            'content'   => $this->getLayout()->createBlock('centrocoste/adminhtml_centroCoste_edit_tab_detail')->toHtml(),
        ));
  
        return parent::_beforeToHtml();
    }
  }
