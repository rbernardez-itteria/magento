<?php

class Itteria_MktpCustomer_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Customer_Grid{

	protected function _prepareColumns(){
		parent::_prepareColumns();
	   	$this->removeColumn('group');
	   	$this->removeColumn('Telephone');
	   	$this->removeColumn('billing_postcode');
	   	$this->removeColumn('billing_country_id');
	   	$this->removeColumn('billing_region');
	   	$this->removeColumn('website_id');
	}

}