<?php

  class Itteria_MktpProduct_Adminhtml_MktpProduct_ImportController extends Mage_Adminhtml_Controller_Action {
    
    // ID del perfil que se crea en el admin
    var $profileId = '7';
    var $fileName = 'products.xml';
    
    /**
     *  Genera el formulario de importación para subir el fichero
     */
    function importAction() {
      
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('mktpproduct/adminhtml_products_edit'));
        $this->_addLeft($this->getLayout()->createBlock('mktpproduct/adminhtml_products_edit_tabs'));
        $this->renderLayout();
    }
    
    /**
     *  Guarda el fichero y lanza proceso de importación
     */
    function saveAction() {

        // Vemos si hay archivo
        if (isset($_FILES['products']['name']) &&
            (file_exists($_FILES['products']['tmp_name'])))
        {
                
            try {
              
                // Cambiamos el nombre al último archivo importado para que el nombre quede libre
                $path = Mage::getBaseDir('var') . DIRECTORY_SEPARATOR . 'import';
                $file = $path . DIRECTORY_SEPARATOR . $this->fileName;
                if (file_exists($file)) {
                    $newName = $path . DIRECTORY_SEPARATOR . date("YmdHis") . '-' . $this->fileName;
                    rename($file, $newName);
                }
                
                // Movemos el archivo subido a la carpeta var/import con el nombre correcto
                $uploader = new Varien_File_Uploader('products');
                $uploader->setAllowedExtensions(array('xml'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('var') . '/import' . DS ;
                $result = $uploader->save($path, $this->fileName);
            }
            catch(Exception $e) {
                Mage::log($e->getMessage());
            }
            
            // Redirect a profile
            Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/system_convert_profile/run", array('id' => $this->profileId)));
        }
    }
  }
