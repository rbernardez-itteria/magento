<?php

class Itteria_MktpProduct_Model_Convert_Adapter_Product extends Mage_Dataflow_Model_Convert_Adapter_Abstract {

    // Aunque no se use, es necesario crear la función vacía
    public function load(){}

    // Aunque no se use, es necesario crear la función vacía
    public function save(){}

    public function saveRow(array $importData){
      
      // Itteria: Obtenemos el producto por sku
      $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$importData['Codi article']);

      // Itteria: Establecemos los valores comunes de los productos  
      /* Itteria: Comentamos el siguiente código para solo dejar la actualización de precios
      $_product->setData('codigo_cpv',$importData['Codi CPV']);
      $_product->setData('desc_cpv',$importData['Descripció CPV']);
      $_product->setName($importData['Descripció article']);
      $_product->setData('unid_venta',$importData['Grup unitats de venda']);
      $_product->setData('fabricante',false);
      $_product->setData('marca',false);
      $_product->setData('modelo',false);
      $_product->setUrlKey($importData['Codi article']);
      */

      // Itteria: Guardamos los cambios en los productos
      /* Itteria: Para actualizar los precios no será necesario guardar el producto en este punto
      $_product->save();
      */

      // Itteria: Recorremos las tiendas y establecemos los valores específico para cada tienda
      $websites = Mage::app()->getWebsites();
      foreach ($websites as $website) {
        $colProveedor = 'Preu ' . $website->getName();
        // Itteria: Comprobamos que tenemos definidos los datos para un proveedor correcto
        if(Mage::getSingleton('admin/session')->getUser()->getRole()->getRoleName() == 'Supervisor' && array_key_exists($colProveedor,$importData)){
          // Itteria: Obtenemos la instancia del producto del proveedor que queremos cambiar
          $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($_product->getId());
          
          // Itteria: Cambiamos todos los atributos
          /* Itteria: Solo vamos a actualizar el precio de los artículos
          $productWebsite->setData('precio_maximo',$importData['Preu ' . $website->getName()]);
          $productWebsite->setData('fabricante',$importData['Fabricant ' . $website->getName()]);
          $productWebsite->setData('marca',$importData['Marca ' . $website->getName()]);
          $productWebsite->setData('modelo',$importData['Model ' . $website->getName()]);
          $productWebsite->setData('precio_maximo',$importData['Preu ' . $website->getName()]);
          */
          try {
            $productWebsite->setPrice($importData['Preu ' . $website->getName()]);
            $productWebsite->save();
          } catch (Exception $e) {
            Mage::log($e->getMessage());
          }       
        }elseif(array_key_exists($colProveedor,$importData) && Mage::getSingleton('admin/session')->getUser()->getRole()->getRoleName() == $website->getName()){
          // Itteria: Obtenemos la instancia del producto del proveedor que queremos cambiar
          $productWebsite = Mage::getModel('catalog/product')->setStoreId($website->getId())->load($_product->getId());
          // Itteria: Cambiamos todos los atributos
          /* Itteria: Solo vamos a actualizar el precio de los artículos
          $productWebsite->setData('precio_maximo',$importData['Preu ' . $website->getName()]);
          $productWebsite->setData('fabricante',$importData['Fabricant ' . $website->getName()]);
          $productWebsite->setData('marca',$importData['Marca ' . $website->getName()]);
          $productWebsite->setData('modelo',$importData['Model ' . $website->getName()]);
          */
          try {
            $productWebsite->setPrice($importData['Preu ' . $website->getName()]);
            $productWebsite->save();
          } catch (Exception $e) {
            Mage::log($e->getMessage());
          }
        }
      }

      
      /* Itteria: Guardamos los datos importado en un log      
          Mage::log(__METHOD__, null, 'import.log');
          Mage::log($importData, null, 'import.log');
      */

      return true;
    }
}