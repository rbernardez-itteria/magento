<?php 

class Itteria_MktpProduct_Model_Observer extends Mage_Core_Model_Abstract {
    
    var $_logKeys = array('name' ,'marca' ,'modelo', 'fabricante', 'price');
    
    /**
     * Valida posibles cambios en un producto
     */
    function validateProductChange($observer) {
        
        //  Itteria: Obtenemos el producto que estamos modificando y su precio máximo
        $product = $observer->getEvent()->getProduct();
        $preciomaximo = $product->getResource()->getAttribute('precio_maximo');

        // Itteria: Si el precio que queremos guardar es mayor al máximo borramos su valor y establecemos el máximo permitido
        if($product->getPrice() > round($preciomaximo->getFrontend()->getValue($product),2)){
            Mage::throwException('El preu no pot superar el preu màxim');
        }
    }
    
    /**
     * Traza cambios en un producto tras guardarlo 
     */
    function logProductChange($observer) {
          
        // Datos del producto
        $product = $observer->getEvent()->getProduct();
          
        // Datos originales y actuales
        $old = $product->getOrigData();
        $new = $product->getData();
        
        // Buscamos cambios
        foreach ($this->_logKeys as $key) {
            
            // Buscamos cambios
            if ($old[$key] != $new[$key]) {
                $this->logChange($product->getId(), $key, $old[$key], $new[$key]);
            }
        }
    }
    
    /**
     * Registra un cambio en un producto
     */
    function logChange($id, $key, $old, $new) {
        
        // Admin user
        $uid = Mage::getSingleton('admin/session')->getUser()->getId();
        
        // Modelo de datos
        $log = Mage::getModel('mktpproduct/log');
        $log->setProductId($id);
        switch ($key) {
            case 'price':
                $log->setEvent('Canvi de preu');
                break;
            case 'name':
                $log->setEvent('Canvi de nom');
                break;
            default:
                $log->setEvent('Canvi de' . $key);
                break;
        }
        
        $log->setValueBefore($old);
        $log->setValueAfter($new);
        $_user = Mage::getModel('admin/user')->load($uid);
        $log->setUserId($_user->getFirstname() . ' ' . $_user->getLastname());
        $log->setMessage('');
        $log->save();
    }
}
