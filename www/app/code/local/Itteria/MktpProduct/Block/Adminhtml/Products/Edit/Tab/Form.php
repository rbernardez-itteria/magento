<?php

class Itteria_MktpProduct_Block_Adminhtml_Products_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        $fieldset = $form->addFieldset('form_form', array('legend'=>Mage::helper('mktpproduct')->__('Pujar arxiu a importar')));
        
        // Comprobamos el estado de la tienda
        if(!Mage::getStoreConfig('mktpsuperorder/itteria_group/shop_mode')){
          // Itteria - Obtenemos el nombre del rol actual
          $roleId = implode('', Mage::getSingleton('admin/session')->getUser()->getRoles());
          $roleName = Mage::getModel('admin/roles')->load($roleId)->getRoleName();

          if(strtolower($roleName) != 'administrators' && strtolower($roleName) != 'supervisor'){
            $fieldset->addField('name', 'note', array(
              'label'     => Mage::helper('mktpproduct')->__('Nom columna Excel'),
              'text'      => Mage::helper('mktpproduct')->__('Preu ' . $roleName),
            ));
          }
          
          $fieldset->addField('note', 'note', array(
            'label'     => Mage::helper('mktpproduct')->__("Tipus d'arxiu"),
            'text'      => Mage::helper('mktpproduct')->__("Pujar un arxiu en format Excel XML 2003/2004.  Un cop pujat l'arxiu, comença el procés d'importació.  Important: No tancar la finestra que s'obre!"),
          ));
            
          $fieldset->addField('products', 'file', array(
            'label'     => Mage::helper('mktpproduct')->__('Arxiu a importar'),
            'required'  => true,
            'name'      => 'products',
          ));
        }else{
          $fieldset->addField('name', 'note', array(
            'label'     => Mage::helper('mktpproduct')->__('Informació'),
            'text'      => Mage::helper('mktpproduct')->__('No es poden actualitzar preus en aquest moment'),
          ));
        }

        return parent::_prepareForm();
    }
}