<?php

class Itteria_MktpProduct_Block_Adminhtml_Products_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'mktpproduct';
        $this->_controller = 'adminhtml_products';
        
        if(Mage::getStoreConfig('mktpsuperorder/itteria_group/shop_mode')){
            $this->_removeButton('save');
        }

        $this->_updateButton('save', 'label', Mage::helper('mktpproduct')->__('Pujar i importar'));
        $this->_updateButton('save', 'target', '_blank');
        $this->_removeButton('back');
    }
 
    public function getHeaderText()
    {
        return Mage::helper('mktpproduct')->__('Importar productes');
    }
}
