<?php

class Itteria_MktpProduct_Block_Adminhtml_Products_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('form_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('mktpproduct')->__('Importar productes'));
  }
 
  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('mktpproduct')->__('Importar productes'),
          'title'     => Mage::helper('mktpproduct')->__('Importar productes'),
          'content'   => $this->getLayout()->createBlock('mktpproduct/adminhtml_products_edit_tab_form')->toHtml(),
      ));

      return parent::_beforeToHtml();
  }
}