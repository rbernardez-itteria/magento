<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$this->getTable(‘log_mktpproduct’)};
    CREATE TABLE {$this->getTable(‘prd_table’)} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `product_id` int(255) NOT NULL DEFAULT ”,
        `event` varchar(255) NOT NULL DEFAULT ”,
        `message` varchar(255) NOT NULL DEFAULT ”,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$installer->endSetup();
