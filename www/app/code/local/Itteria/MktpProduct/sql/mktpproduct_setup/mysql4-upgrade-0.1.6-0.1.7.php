<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `log_mktpproduct` ADD user_id VARCHAR(64) AFTER event;
    ALTER TABLE `log_mktpproduct` ADD value_before VARCHAR(64) AFTER user_id;
    ALTER TABLE `log_mktpproduct` ADD value_after VARCHAR(64) AFTER value_before;
");

$installer->endSetup();