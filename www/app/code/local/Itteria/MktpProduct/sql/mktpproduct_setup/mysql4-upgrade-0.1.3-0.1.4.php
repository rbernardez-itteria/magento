<?php

$installer = $this;
$installer->startSetup();


$w = $this->_conn;
$w->addColumn($installer->getTable('sales/shipment_item'), 
              'aceptado', 
              array(
                    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => true,
                    'default' => 'Pendent',
                    'comment' => 'Shipment aceptado',
                    )
              );
    
$installer->endSetup();