<?php

class Itteria_MktpExportGrid_Block_Adminhtml_Widget_Grid extends Mage_Adminhtml_Block_Widget_Grid{

	function _exportCsvItem(Varien_Object $item, Varien_Io_File $adapter){
		$row = array();
		foreach ($this->_columns as $column) {
			if (!$column->getIsSystem()) {
				$row[] = $column->getRowFieldExport($item);
			}
		}
		$row = $this->d_encodeUTF8($row);
		$adapter->streamWriteCsv($row);
	}
	
	 function d_encodeUTF8($array){
		foreach($array as $key=>$value){
			$array[$key] = !is_array($value)?iconv("UTF-8", "ISO-8859-1//TRANSLIT",$value):$this->d_encodeUTF8($array[$key]);
		}
		 
		return $array;
	}

	public function getCsvFile()
	{
	    $this->_isExport = true;
	    $this->_prepareGrid();

	    $io = new Varien_Io_File();

	    $path = Mage::getBaseDir('var') . DS . 'export' . DS;
	    $name = md5(microtime());
	    $file = $path . DS . $name . '.csv';

	    $io->setAllowCreateFolders(true);
	    $io->open(array('path' => $path));
	    $io->streamOpen($file, 'w+');
	    $io->streamLock(true);
	    $io->streamWriteCsv($this->_getExportHeadersCSV());

	    $this->_exportIterateCollection('_exportCsvItem', array($io));

	    if ($this->getCountTotals()) {
	        $io->streamWriteCsv($this->_getExportTotals());
	    }

	    $io->streamUnlock();
	    $io->streamClose();

	    return array(
	        'type'  => 'filename',
	        'value' => $file,
	        'rm'    => true // can delete file after use
	    );
	}
	
	 function _getExportHeadersCSV(){
		$row = array();
		foreach ($this->_columns as $column) {
			if (!$column->getIsSystem()) {
				$row[] = $column->getExportHeader();
			}
		}
		$row = $this->d_encodeUTF8($row);
		return $row;
	}
	
	 function _getExportTotals(){
		$totals = $this->getTotals();
		$row    = array();
		foreach ($this->_columns as $column) {
			if (!$column->getIsSystem()) {
				$row[] = ($column->hasTotalsLabel()) ? $column->getTotalsLabel() : $column->getRowFieldExport($totals);
			}
		}
		$row = $this->d_encodeUTF8($row);
		return $row;
	}


}