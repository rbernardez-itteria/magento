<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('adminhtml')->__('Usuaris'));
    }
   
    protected function _beforeToHtml()
    {
        $this->addTab('form_detail', array(
            'label'     => Mage::helper('adminhtml')->__("Dades de l'usuari"),
            'title'     => Mage::helper('adminhtml')->__("Dades de l'usuari"),
            'content'   => $this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpUsersProveedores_edit_tab_detail')->toHtml(),
        ));
  
        return parent::_beforeToHtml();
    }
  }