<?php

class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_Renderer_Proveedor extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input{

	public function render(Varien_Object $row){

		// Itteria - Obtenemos el rol asociado al usuario
		$rol = Mage::getModel('admin/role')->getCollection()->addFieldToFilter('user_id',$row->getUserId());
        $rolData = $rol->getData();
		// Itteria - Obtenemos el rol avanzado asociado a dicho usuario
		$rol_advanced = Mage::getModel('aitpermissions/advancedrole')->getCollection()->loadByRoleId($rolData[0]['parent_id']);
        $rol_advancedData = $rol_advanced->getData();
         
		// Itteria - Mostramo el nombre de la tienda que tiene asociada
		$store = Mage::getModel('core/store')->load($rol_advancedData[0]['website_id']);
		
		return $store->getName();
	}
}