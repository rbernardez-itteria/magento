<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores_New_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form {
      
      protected function _prepareForm(){

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Account Information')));

        $fieldset->addField('form_type', 'hidden', array(
          'name'      => 'form_type',
          'value'     => 'new'
        ));

        $fieldset->addField('proveedor', 'text', array(
            'name'  => 'proveedor',
            'label' => Mage::helper('adminhtml')->__('Proveïdor'),
            'title' => Mage::helper('adminhtml')->__('Proveïdor'),
            'required' => true,
            'value'     => '',
          )
        );

        $fieldset->addField('nif', 'text', array(
            'name'  => 'nif',
            'label' => Mage::helper('adminhtml')->__('NIF'),
            'title' => Mage::helper('adminhtml')->__('NIF'),
            'required' => true,
            'value'     => '',
          )
        );

        return parent::_prepareForm();
      }
  }