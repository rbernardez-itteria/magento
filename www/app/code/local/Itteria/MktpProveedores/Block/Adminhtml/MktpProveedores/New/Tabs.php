<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores_New_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('form_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('adminhtml')->__('Proveïdors'));
    }
   
    protected function _beforeToHtml()
    {
        $this->addTab('form_detail', array(
            'label'     => Mage::helper('adminhtml')->__('Dades del proveïdor'),
            'title'     => Mage::helper('adminhtml')->__('Dades del proveïdor'),
            'content'   => $this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpProveedores_new_tab_detail')->toHtml(),
        ));
  
        return parent::_beforeToHtml();
    }
  }