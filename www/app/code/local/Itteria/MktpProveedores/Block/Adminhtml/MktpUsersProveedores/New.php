<?php

class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'mktpproveedores';
        $this->_controller = 'adminhtml_mktpUsersProveedores';

        $this->_removeButton('reset');
        $this->_removeButton('delete');
    }
 
    public function getHeaderText()
    {
        return Mage::helper('adminhtml')->__('Nou usuari');
    }
}
