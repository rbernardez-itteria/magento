<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_New_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form {
      
      protected function _prepareForm(){

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Account Information')));

        $fieldset->addField('form_type', 'hidden', array(
          'name'      => 'form_type',
          'value'     => 'new'
        ));

        $fieldset->addField('username', 'text', array(
            'name'  => 'username',
            'label' => Mage::helper('adminhtml')->__("Nom de l'usuari"),
            'title' => Mage::helper('adminhtml')->__("Nom de l'usuari"),
            'required' => true,
            'value' => '',
            )
        );

        $fieldset->addField('lastname', 'text', array(
            'name'  => 'lastname',
            'label' => Mage::helper('adminhtml')->__("Nom"),
            'title' => Mage::helper('adminhtml')->__("Nom"),
            'required' => true,
            'value' => '',
            )
        );

        $fieldset->addField('email', 'text', array(
            'name'  => 'email',
            'label' => Mage::helper('adminhtml')->__("Email"),
            'title' => Mage::helper('adminhtml')->__("Email"),
            'required' => true,
            'value' => '',
            )
        );

        $fieldset->addField('password', 'password',
            array(
                'name'  => 'password',
                'label' => Mage::helper('adminhtml')->__('Password'),
                'id'    => 'password',
                'title' => Mage::helper('adminhtml')->__('Password'),
                'required' => true,
                'value' => '',
                'class' => 'input-text validate-password',
            )
        );

        $fieldset->addField('confirmation', 'password',
            array(
                'name'  => 'password_confirmation',
                'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
                'id'    => 'confirmation',
                'required' => true,
                'value' => '',
                'class' => 'input-text validate-cpassword',
            )
        );

        $proveedores = array();
        $role_collection = Mage::getModel('aitpermissions/advancedrole')->getCollection();
        foreach ($role_collection as $role){
            $website = Mage::getModel('core/website')->load($role->getWebsiteId());
            if($website->getName() != 'Genérica'){
               $proveedores[$website->getName()] = $website->getName(); 
            }
        }

        $fieldset->addField('proveedor', 'select', array(
            'name'  => 'proveedor[]',
            'label' => Mage::helper('adminhtml')->__("Proveïdor"),
            'title' => Mage::helper('adminhtml')->__("Proveïdor"),
            'required' => true,
            'values' => $proveedores,
            )
        );

        $fieldset->addField('is_active', 'select', array(
            'name'  => 'is_active',
            'label' => Mage::helper('adminhtml')->__("Estat"),
            'title' => Mage::helper('adminhtml')->__("Estat"),
            'required' => true,
            'class'     => 'input-select',
            'options'    => array('0' => Mage::helper('adminhtml')->__('Active'), '1' => Mage::helper('adminhtml')->__('Inactive')),
            )
        );

        return parent::_prepareForm();
      }
  }