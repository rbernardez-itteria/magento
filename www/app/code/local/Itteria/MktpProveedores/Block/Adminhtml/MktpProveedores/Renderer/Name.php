<?php

class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores_Renderer_Name extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input{

	public function render(Varien_Object $row){

		// Itteria - Mostramo el nombre de la tienda que tiene asociada
		$store = Mage::getModel('core/store')->load($row->getWebsiteId());
		return $store->getName();
	}
}