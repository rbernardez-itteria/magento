<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores_Edit_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form {
      
      protected function _prepareForm(){

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Account Information')));

        // Itteria - Mostramo el nombre de la tienda que tiene asociada
        $store = Mage::getModel('core/store')->load($this->getRequest()->getParam('website_id'));

        $fieldset->addField('id_store', 'hidden', array(
            'name'  => 'id_store',
            'value' => $store->getId(),
            )
        );

        $fieldset->addField('proveedor', 'text', array(
            'name'  => 'proveedor',
            'label' => Mage::helper('adminhtml')->__('Proveïdor'),
            'title' => Mage::helper('adminhtml')->__('Proveïdor'),
            'required' => true,
            'value' => $store->getName(),
          )
        );

        $fieldset->addField('nif', 'text', array(
            'name'  => 'nif',
            'label' => Mage::helper('adminhtml')->__('NIF'),
            'title' => Mage::helper('adminhtml')->__('NIF'),
            'required' => true,
            'value' => $store->getGroup()->getName(),
          )
        );

        return parent::_prepareForm();
      }
  }