<?php
class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct(){
        parent::__construct();
        
        $this->setDefaultSort('username');
        $this->setId('mktpproveedores_mktpusersSproveedores_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('admin/user')->getCollection();
        $collection
            ->addFieldToFilter('firstname', array('eq' => 'proveedor'));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){

        $this->addColumn('username', array(
            'header' => Mage::helper('adminhtml')->__("Nom de l'usuari"),
            'index' => 'username',
        ));

        $this->addColumn('lastname', array(
            'header' => Mage::helper('adminhtml')->__('Nom'),
            'index' => 'lastname',
        ));

        $this->addColumn('email', array(
            'header' => Mage::helper('adminhtml')->__('Correu electrònic'),
            'index' => 'email',
        ));

        $this->addColumn('proveedor', array(
            'header' => Mage::helper('adminhtml')->__('Proveïdor'),
            'renderer' => 'mktpproveedores/adminhtml_mktpUsersProveedores_renderer_proveedor',
            'index' => 'user_id',
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('adminhtml')->__('Status'),
            'index'     => 'is_active',
            'align'     =>'left',
            'type'      => 'options',
            'options'   => array('1' => Mage::helper('adminhtml')->__('Active'), '0' => Mage::helper('adminhtml')->__('Inactive')),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row){
        return $this->getUrl('*/*/edit', array('user_id' => $row->getId()));
    }
}
