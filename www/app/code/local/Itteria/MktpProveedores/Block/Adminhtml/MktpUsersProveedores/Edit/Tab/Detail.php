<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores_Edit_Tab_Detail extends Mage_Adminhtml_Block_Widget_Form {
      
      protected function _prepareForm(){

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Account Information')));

        // Itteria - Mostramo el nombre de la tienda que tiene asociada
        $user = Mage::getModel('admin/user')->load($this->getRequest()->getParam('user_id'));

        $fieldset->addField('id_user', 'hidden', array(
            'name'  => 'id_user',
            'value' => $user->getId(),
            )
        );

        $fieldset->addField('username', 'text', array(
            'name'  => 'username',
            'label' => Mage::helper('adminhtml')->__("Nom de l'usuari"),
            'title' => Mage::helper('adminhtml')->__("Nom de l'usuari"),
            'required' => true,
            'value' => $user->getUsername(),
            )
        );

        $fieldset->addField('lastname', 'text', array(
            'name'  => 'lastname',
            'label' => Mage::helper('adminhtml')->__("Nom"),
            'title' => Mage::helper('adminhtml')->__("Nom"),
            'required' => true,
            'value' => $user->getLastname(),
            )
        );

        $fieldset->addField('email', 'text', array(
            'name'  => 'email',
            'label' => Mage::helper('adminhtml')->__("Email"),
            'title' => Mage::helper('adminhtml')->__("Email"),
            'required' => true,
            'value' => $user->getEmail(),
            )
        );

        $fieldset->addField('password', 'password',
            array(
                'name'  => 'password',
                'label' => Mage::helper('adminhtml')->__('Password'),
                'id'    => 'password',
                'title' => Mage::helper('adminhtml')->__('Password'),
                'required' => true,
                'value' => $user->getPassword(),
                'class' => 'input-text validate-password',
            )
        );

        $fieldset->addField('confirmation', 'password',
            array(
                'name'  => 'password_confirmation',
                'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
                'id'    => 'confirmation',
                'required' => true,
                'value' => $user->getPassword(),
                'class' => 'input-text validate-cpassword',
            )
        );

        $proveedores = array();
        $role_collection = Mage::getModel('aitpermissions/advancedrole')->getCollection();
        $userRolData = $user->getRole()->getData(); 
        $rol_actual = $userRolData['role_name'];
        Mage::log($userRolData['role_name']);
        $proveedores[$rol_actual] = $rol_actual;
        foreach ($role_collection as $role){
            $website = Mage::getModel('core/website')->load($role->getWebsiteId());
            if($website->getName() != 'Genérica'){
               $proveedores[$website->getName()] = $website->getName(); 
            }
        }
        $fieldset->addField('proveedor', 'select', array(
            'name'  => 'proveedor[]',
            'label' => Mage::helper('adminhtml')->__("Proveïdor"),
            'title' => Mage::helper('adminhtml')->__("Proveïdor"),
            'required' => true,
            'value' => $rol_actual,
            'options' => $proveedores,
            )
        );

        if($user->getIsActive()){
            $fieldset->addField('is_active_actived', 'select', array(
                'name'  => 'is_active_actived',
                'label' => Mage::helper('adminhtml')->__("Estat"),
                'title' => Mage::helper('adminhtml')->__("Estat"),
                'required' => true,
                'class'     => 'input-select',
                'options'    => array('activo' => Mage::helper('adminhtml')->__('Active'), 'desactivo' => Mage::helper('adminhtml')->__('Inactive')),
                )
            );
        }else{
            $fieldset->addField('is_active_desactived', 'select', array(
                'name'  => 'is_active_desactived',
                'label' => Mage::helper('adminhtml')->__("Estat"),
                'title' => Mage::helper('adminhtml')->__("Estat"),
                'required' => true,
                'class'     => 'input-select',
                'options'    => array('desactivo' => Mage::helper('adminhtml')->__('Inactive'), 'activo' => Mage::helper('adminhtml')->__('Active')),
                )
            );
        }

        return parent::_prepareForm();
    }
}