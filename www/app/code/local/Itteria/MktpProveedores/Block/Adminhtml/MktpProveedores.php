<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
      
        $this->_blockGroup = 'mktpproveedores';
        $this->_controller = 'adminhtml_mktpProveedores';
        $this->_headerText = $this->__('Proveïdors');
        parent::__construct();
    }
  }