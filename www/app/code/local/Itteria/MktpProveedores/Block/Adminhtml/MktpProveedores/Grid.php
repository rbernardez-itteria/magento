<?php
class Itteria_MktpProveedores_Block_Adminhtml_MktpProveedores_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct(){
        parent::__construct();

        $this->setDefaultSort('proveedores');
        $this->setId('mktpproveedores_mktpproveedores_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('core/store')->getCollection();
        $collection->addFieldToFilter('store_id', array('neq' => 1));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){

        $this->addColumn('proveedores', array(
            'header' => Mage::helper('adminhtml')->__('Proveïdor'),
            'renderer' => 'mktpproveedores/adminhtml_mktpProveedores_renderer_name',
            'index' => 'website_id',
        ));

        $this->addColumn('nif', array(
            'header' => Mage::helper('adminhtml')->__('NIF'),
            'renderer' => 'mktpproveedores/adminhtml_mktpProveedores_renderer_nif',
            'index' => 'website_id',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row){
        return $this->getUrl('*/*/edit', array('website_id' => $row->getId()));
    }
}
