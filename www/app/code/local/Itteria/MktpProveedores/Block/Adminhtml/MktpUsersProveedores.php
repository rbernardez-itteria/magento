<?php

  class Itteria_MktpProveedores_Block_Adminhtml_MktpUsersProveedores extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
      
        $this->_blockGroup = 'mktpproveedores';
        $this->_controller = 'adminhtml_mktpUsersProveedores';
        $this->_headerText = $this->__('Usuaris');
        parent::__construct();
    }
  }