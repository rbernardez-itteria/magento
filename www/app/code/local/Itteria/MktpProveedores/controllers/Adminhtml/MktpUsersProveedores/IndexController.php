<?php

class Itteria_MktpProveedores_Adminhtml_MktpUsersProveedores_IndexController extends Mage_Adminhtml_Controller_Action {
    
	function indexAction() {    
	    $this->loadLayout();
	    $this->renderLayout();
	}

	function editAction() {
	    $this->loadLayout();
	    $this->_addContent($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpUsersProveedores_edit'));
	    $this->_addLeft($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpUsersProveedores_edit_tabs'));
	    $this->renderLayout();
	}

	function newAction() {
	    $this->loadLayout();
	    $this->_addContent($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpUsersProveedores_new'));
	    $this->_addLeft($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpUsersProveedores_new_tabs'));
	    $this->renderLayout();
	}

	function saveAction(){

		// Itteria - Tipo de envío
		$type = $this->getRequest()->getPost('form_type');
        $proveedor = $this->getRequest()->getPost('proveedor');
		$website = Mage::getModel('core/website')->getCollection()->addFieldToFilter('name', $proveedor[0]);
        $websiteData = $website->getData(); 
		$rol_avanzado = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id', $websiteData[0]['website_id']);
        $rol_avanzadoData = $rol_avanzado->getData(); 
		$rol = Mage::getModel('admin/roles')->load($rol_avanzadoData[0]['role_id']);

		if ($type == 'new') {
			try{
				$user = Mage::getModel('admin/user');
				$user->setUsername($this->getRequest()->getPost('username'));
				$user->setFirstname('Proveedor');
				$user->setLastname($this->getRequest()->getPost('lastname'));
				$user->setEmail($this->getRequest()->getPost('email'));
				$user->setIsActive(1);
				$user->setPassword($this->getRequest()->getPost('password'));

				$user->save();
			}catch(Exception $e){
				Mage::getSingleton('adminhtml/session')->addSuccess($e->getMessage());
			}

			try {
			    $user->setRoleIds(array($rol->getId()))->setRoleUserId($user->getUserId())->saveRelations();
			}catch (Exception $e) {
			    echo $e->getMessage();
			    exit;
			}

			// Itteria - Mostramos exito
			Mage::getSingleton('adminhtml/session')->addSuccess('Usuari creat correctament');
			
		}else{

			$user = Mage::getModel('admin/user')->load($this->getRequest()->getPost('id_user'));
			
			$user->setUsername($this->getRequest()->getPost('username'));
			$user->setFirstname('Proveedor');
			$user->setLastname($this->getRequest()->getPost('lastname'));
			$user->setEmail($this->getRequest()->getPost('email'));
			$user->setPassword($this->getRequest()->getPost('password'));
			
			if($this->getRequest()->getPost('is_active_actived') != null){
				$this->getRequest()->getPost('is_active_actived') == 'activo' ? $user->setIsActive(1) : $user->setIsActive(0);
			}
			if($this->getRequest()->getPost('is_active_desactived') != null){
				$this->getRequest()->getPost('is_active_desactived') == 'desactivo' ? $user->setIsActive(0) : $user->setIsActive(1);
			}

			$user->save();

			$user->setRoleIds(array($rol->getId()))->setRoleUserId($user->getUserId())->saveRelations();

			// Mostramos exito
			Mage::getSingleton('adminhtml/session')->addSuccess('Usuari modificat correctament');
		}

		// Vamos al detalle de la cuenta
		Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/mktpUsersProveedores_index/index'));
	}
}