<?php

class Itteria_MktpProveedores_Adminhtml_MktpProveedores_IndexController extends Mage_Adminhtml_Controller_Action {
    
	function indexAction() {    
	    $this->loadLayout();
	    $this->renderLayout();
	}

	function editAction() {
	    $this->loadLayout();
	    $this->_addContent($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpProveedores_edit'));
	    $this->_addLeft($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpProveedores_edit_tabs'));
	    $this->renderLayout();
	}

	function newAction() {
	    $this->loadLayout();
	    $this->_addContent($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpProveedores_new'));
	    $this->_addLeft($this->getLayout()->createBlock('mktpproveedores/adminhtml_mktpProveedores_new_tabs'));
	    $this->renderLayout();
	}

	function saveAction(){
		
		// Itteria - Tipo de envío
		$type = $this->getRequest()->getPost('form_type');
		
		if ($type == 'new') {
		  	
			// Itteria - Creación de la tienda
			$store_name = $this->getRequest()->getPost('proveedor');
			$store_code = str_replace(',','',$store_name);
			$store_code = preg_replace('/[^A-Za-z0-9\-]/', '', $store_code);
			$store_code = 'proveedor_' . str_replace(' ','_',$store_code);
			$store_code = strtolower($store_code);
			$store_view = 'view_' . $store_code;

			// Itteria - Creación del Website
		    $website = Mage::getModel('core/website');
		    $website->setCode($store_code)
		        ->setName($store_name)
		        ->save();

		    // Itteria - Creación de la Store
		    $storeGroup = Mage::getModel('core/store_group');
		    $storeGroup->setWebsiteId($website->getId())
		        ->setName($this->getRequest()->getPost('nif'))
		        ->setRootCategoryId('2')
		        ->save();

		    // Itteria - Creación de la vista de la store
		    $store = Mage::getModel('core/store');
		    $store->setCode($store_view)
		        ->setWebsiteId($storeGroup->getWebsiteId())
		        ->setGroupId($storeGroup->getId())
		        ->setName($store_name)
		        ->setIsActive(1)
		        ->save();

		    // Itteria - Creación del rol de usuario
		    $rolBaseId = 82;
		    $roleModel    = Mage::getModel('admin/roles');
		    $aitRoleModel = Mage::getModel('aitpermissions/advancedrole');
		    $loadRole     = $roleModel->load(82);
		    $roleName     = $loadRole->getRoleName();
		    $ruleModel    = Mage::getModel("admin/rules");
		    $loadRuleCollection = $ruleModel->getCollection()->addFilter('role_id',82);
		    $loadAitRoleCollection  = $aitRoleModel->getCollection()->addFilter('role_id',82);
		    
		    try
		    {
		        $roleModel->setId(null)
		            ->setName($store_name)
		            ->setPid($loadRole->getParentId())
		            ->setTreeLevel($loadRole->getTreeLevel())
		            ->setType($loadRole->getType())
		            ->setUserId($loadRole->getUserId())
		         ->save();
		        
		        foreach ($loadRuleCollection as $rule)
		        {
		            $ruleModel
		                ->setData($rule->getData())
		                ->setRuleId(null)
		                ->setRoleId($roleModel->getId())
		            ->save();
		        }
		        $newRoleId =  $roleModel->getRoleId();
		        foreach ($loadAitRoleCollection as $loadAitRole)
		        {
		            $aitRoleModel->setId(null)
		                ->setRoleId($newRoleId)
		                ->setWebsiteId($website->getId())
		                ->setStoreId($loadAitRole->getStoreId())
		                ->setStoreviewIds($loadAitRole->getStoreviewIds())
		                ->setCategoryIds($loadAitRole->getCategoryIds())
		                ->setCanEditGlobalAttr(	$loadAitRole->getCanEditGlobalAttr())
		                ->setCanEditOwnProductsOnly($loadAitRole->getCanEditOwnProductsOnly())
		                ->setCanCreateProducts($loadAitRole->getCanCreateProducts())
		                ->setManageOrdersOwnProductsOnly($loadAitRole->getManageOrdersOwnProductsOnly())
		            ->save();
		        }

		        Mage::getSingleton('aitpermissions/editor_attribute')->getCollection()->duplicateAttributePermissions(82, $newRoleId);
		        Mage::getSingleton('aitpermissions/editor_type')->getCollection()->duplicateProductTypePermissions(82, $newRoleId);
		        Mage::getSingleton('aitpermissions/editor_tab')->getCollection()->duplicateProductTabPermissions(82, $newRoleId);
		    }
		    catch (Exception $e)
		    {
		        $this->_getSession()->addError($this->__("Role %s wasn't duplicated. %s",$roleName,$e->getMessage()));
		    }

		    // Itteria - Realizamos el reindex
		    $indexCollection = Mage::getModel('index/process')->getCollection();
		    foreach ($indexCollection as $index) {
		        $index->reindexAll();
		    }

			// Mostramos exito
			Mage::getSingleton('adminhtml/session')->addSuccess('Proveïdor creat correctament');
		}
		else {

			// Itteria - Obtenemos los datos
			$store_name = $this->getRequest()->getPost('proveedor');
			$store_code = str_replace(',','',$store_name);
			$store_code = preg_replace('/[^A-Za-z0-9\-]/', '', $store_code);
			$store_code = 'proveedor_' . str_replace(' ','_',$store_code);
			$store_code = strtolower($store_code);
			$store_view = 'view_' . $store_code;

			// Itteria - Actualizamos el website
		    $website = Mage::getModel('core/website')->load($this->getRequest()->getPost('id_store'));
		    $website->setCode($store_code)
		        ->setName($store_name)
		        ->save();

		    // Itteria - Actualizamos la tienda
		    $storeGroup = Mage::getModel('core/store_group')->load($this->getRequest()->getPost('id_store'));
		    $storeGroup->setWebsiteId($website->getId())
		        ->setName($this->getRequest()->getPost('nif'))
		        ->save();

		    // Itteria - Actualizamos la vista de la tienda
		    $store = Mage::getModel('core/store')->load($this->getRequest()->getPost('id_store'));
		    $store->setCode($store_view)
		        ->setName($store_name)
		        ->save();

		    // Itteria - Actualizamos el rol avanzado
		    $rol_avanzado = Mage::getModel('aitpermissions/advancedrole')->getCollection()->addFieldToFilter('website_id',$this->getRequest()->getPost('id_store'));
            $rol_avanzadoData = $rol_avanzado->getData();
		    $rol = Mage::getModel('admin/roles')->load($rol_avanzadoData[0]['role_id']);
		    $rol->setName($store_name);
		    $rol->save();

		    // Itteria - Realizamos el reindex
		    $indexCollection = Mage::getModel('index/process')->getCollection();
		    foreach ($indexCollection as $index) {
		        $index->reindexAll();
		    }

		  // Mostramos exito
		  Mage::getSingleton('adminhtml/session')->addSuccess('Proveïdor modificat correctament');
		}
		Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('*/mktpProveedores_index/index'));
	}
}
