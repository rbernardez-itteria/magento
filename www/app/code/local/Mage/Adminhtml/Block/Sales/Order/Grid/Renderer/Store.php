<?php
/**
 * Renderer for Handling field in sales order grid
 */
class Mage_Adminhtml_Block_Sales_Order_Grid_Renderer_Store
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{

    /**
     * Render field
     */
    public function render(Varien_Object $row)
    {
        return Mage::getModel('core/store')->load($row['store_id'])->getName();
    }
}
