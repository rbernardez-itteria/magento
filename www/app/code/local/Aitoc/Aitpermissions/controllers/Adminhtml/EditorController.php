<?php
/**
 * Advanced Permissions
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpermissions
 * @version      2.10.7
 * @license:     wqPlKGYZrcteWDZ3MWJBZVlMr6fjOASxcx9WX0qkok
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitpermissions_Adminhtml_EditorController extends Mage_Adminhtml_Controller_Action
{
    public function attributegridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('aitpermissions/adminhtml_permissions_tab_product_editor_attribute')->toHtml()
        );
    }
}