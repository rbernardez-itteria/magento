/* Inicialitzaci� en catal� per a l'extensi� 'UI date picker' per jQuery. */
/* Writers: (joan.leon@gmail.com). */
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "../datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}(function( datepicker ) {

datepicker.regional['ca'] = {
	closeText: 'Tanca',
	prevText: 'Anterior',
	nextText: 'Seg�ent',
	currentText: 'Avui',
	monthNames: ['Gener','Febrer','Mar�','Abril','Maig','Juny',
	'Juliol','Agost','Setembre','Octubre','Novembre','Desembre'],
	monthNamesShort: ['Gen','Feb','Mar�','Abr','Maig','Juny',
	'Jul','Ag','Set','Oct','Nov','Des'],
	dayNames: ['diumenge','dilluns','dimarts','dimecres','dijous','divendres','dissabte'],
	dayNamesShort: ['Diu','Dil','Dim','Dim','Dij','Div','Dis'],
	dayNamesMin: ['Diu','Dil','Dim','Dim','Dij','Div','Dis'],
	weekHeader: 'Set',
	dateFormat: 'dd/mm/yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ca']);

return datepicker.regional['ca'];

}));